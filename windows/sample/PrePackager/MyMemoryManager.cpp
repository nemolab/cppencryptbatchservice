#include "MyMemoryManager.h"

#include "stdafx.h"
#include "PrePackager.h"
#include "PrePackagerDlg.h"

#include <aws/core/utils/memory/MemorySystemInterface.h>
#include <aws/core/utils/memory/AWSMemory.h>

class MyMemoryManager : public Aws::Utils::Memory::MemorySystemInterface
{
public:
	MyMemoryManager() {}
	virtual ~MyMemoryManager() {}

	// 커스텀 메모리 매니저 시작과 끝에서 불리는 함수. 필요시 구현할 것
	//virtual void Begin() override {}
	//virtual void End() override {}

	// 할당(new)시 불리는 AllocateMemory 인터페이스 구현
	virtual void* AllocateMemory(std::size_t blockSize, std::size_t alignment, const char *allocationTag = nullptr) override
	{
		// 여기에 메모리 할당 로직 직접 구현
		// 예: tcmalloc을 사용한다면, tc_malloc(blocksize);
		// 예: UnrealEngine에서 제공하는 할당자를 사용한다면, FMemory::Malloc()

		return _aligned_malloc(blockSize, alignment);
	}

	// 해제(delete)시 불리는 FreeMemory 인터페이스 구현
	virtual void FreeMemory(void* memoryPtr) override
	{
		// 여기에 메모리 해제 로직 직접 구현
		// 예: tcmalloc을 사용한다면, tc_free(memoryPtr)
		// 예: UnrealEngine의 경우는, FMalloc::Free()

		_aligned_free(memoryPtr);
	}

};