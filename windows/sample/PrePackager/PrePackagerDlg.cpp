
// PrePackagerDlg.cpp : 구현 파일
//



#include "stdafx.h"
#include "PrePackager.h"
#include "PrePackagerDlg.h"
#include "Wininet.h"    
#include <strsafe.h>
#undef GetMessage          // workaround for AWSError method GetMessage(), and ATL conflict
#undef GetObject           // workaround for Aws::S3::S3Client::GetObject

#include <thread>

#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/CreateBucketRequest.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <aws/s3/model/GetObjectRequest.h>
#include <aws/s3/model/Permission.h>
#include <aws/s3/model/Grantee.h>
#include <aws/s3/model/GetObjectAclRequest.h>
#include <aws/s3/model/PutObjectAclRequest.h>
#include <aws/s3/model/ObjectCannedACL.h>
//#include <aws/cognito-identity/model/Credentials.h>
//#include <aws/cognito-identity/CognitoIdentityClient.h>
//#include <aws/core/utils/json/JsonSerializer.h>
#include <aws/core/auth/AWSCredentialsProvider.h>
#include <aws/identity-management/auth/CognitoCachingCredentialsProvider.h>
#include <aws/dynamodb/DynamoDBClient.h>

#include <aws/core/utils/memory/stl/AwsStringStream.h> 

#include <aws/core/Core_EXPORTS.h>

#include <aws/core/utils/logging/FormattedLogSystem.h>

#include <aws/transfer/TransferManager.h>

#include <aws/core/client/ClientConfiguration.h>
#include <aws/core/http/Scheme.h>
#include <aws/core/utils/threading/Executor.h>

#include <iostream>
#include <fstream>

#include <mutex>
#include "json/json.h"




using namespace std;
using namespace Aws::S3;
using namespace Aws::S3::Model;
using namespace Aws::Client;
using namespace Aws::Http;

struct MemoryStruct {
	char *memory;
	size_t size;
};


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

#define	TMS4_CRYPTO_BUFFER_SIZE						64 * 1024
#define	KEY_TYPE_MAC								600
#define	KEY_TYPE_STREAMING							601
#define TMS4_HINT_SOURCE_FILE_SIZE					0x00000001
#define TMS4_HINT_MAC_ADDRESS						0x00000010
#define TMS4_HINT_STREAMING							0x00001000
#define BATCHJOB_TIMER 100
#define ERR_NO_MORE_LIST 4301
#define ERR_S3_DOWNLOAD 4302


//----------------------------------------------------------------------------------------------------------------------
// DRM 
typedef int ( *pfnTMS4XInitInstance ) ( TCHAR* );
typedef void ( *pfnTMS4XFinalize )();
typedef int ( *pfnTMS4XGetHDDSerialNumber ) ( TCHAR*, int );
typedef int ( *pfnTMS4XGetMacAddress )( TCHAR *, int);
typedef int ( *pfnTMS4XFileEncryptEx )( const TCHAR*, const TCHAR*, int, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, int, int, int );
typedef int ( *pfnTMS4XFileDecryptEx )( const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*);
typedef int ( *pfnTMS4XFileEncrypt )( const TCHAR*, const TCHAR*, int, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, int, int, int, WORD );
typedef int ( *pfnTMS4XFileDecrypt )( const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*);
typedef int ( *pfnTMS4XPrePackLocalRecrypt )( const TCHAR*, int, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, int, int, int, const TCHAR*, WORD);

typedef int ( *pfnTMS4ECreateFile )( const TCHAR*, BOOL, HANDLE );
typedef int ( *pfnTMS4EWriteFile )( HANDLE, LPVOID, DWORD, LPDWORD );
typedef int ( *pfnTMS4EMakeFooterNemo )( HANDLE, int, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, int, int, int );
typedef int ( *pfnTMS4ECloseHandle )( HANDLE );
typedef int ( *pfnTMS4EGetResumeFileSize )( HANDLE, PULONGLONG );
typedef int ( *pfnTMS4EGetMacAddress )( TCHAR* );
typedef int ( *pfnTMS4EPrePackLocalRecryptEx )( const TCHAR*, int, const TCHAR*, const TCHAR*, const TCHAR*, const TCHAR*, int, int, int, const TCHAR*, WORD );

pfnTMS4XInitInstance			TMS4XInit = NULL;
pfnTMS4XFinalize				TMS4XFinal = NULL;
pfnTMS4XGetHDDSerialNumber		TMS4XGetHDDSerialNumber = NULL;
pfnTMS4XGetMacAddress			TMS4XGetMacAddress = NULL;
pfnTMS4XFileEncryptEx			TMS4XFileEncryptEx = NULL;
pfnTMS4XFileDecryptEx			TMS4XFileDecryptEx = NULL;
pfnTMS4XFileEncrypt				TMS4XFileEncrypt = NULL;
pfnTMS4XFileDecrypt				TMS4XFileDecrypt = NULL;
pfnTMS4XPrePackLocalRecrypt		TMS4XPrepackLocalRecrypt = NULL;

pfnTMS4ECreateFile				TMS4CreateFile = NULL;
pfnTMS4EWriteFile				TMS4WriteFile = NULL;
pfnTMS4EMakeFooterNemo			TMS4MakeFooterNemo = NULL;
pfnTMS4ECloseHandle				TMS4CloseHandle = NULL;
pfnTMS4EGetResumeFileSize		TMS4GetResumFileSize = NULL;
pfnTMS4EGetMacAddress			TMS4GetMacAddress = NULL;
pfnTMS4EPrePackLocalRecryptEx	TMS4PrepackLocalRecryptEx = NULL;

static const char* KEY = "AKIAIBGUWTGELLH3QNPA";
static const char* BUCKET = "";
static const char* const ALLOCATION_TAG = "S3_MULTIPART_TEST";
static const char* const COGNITO_ALLOCATION_TAG = "S3CLIENT_WITH_COGNITO_TEST";
//----------------------------------------------------------------------------------------------------------------------


unsigned WINAPI WaitCopyThreadFunc( void* pArguments )
{
	if( pArguments != NULL )
	{
		DWORD dwWait = 0;
		dwWait = WaitForSingleObject( ((HANDLE_SET *)pArguments)->hCopyThread,INFINITE );
		if( dwWait == WAIT_OBJECT_0 )
		{
			DWORD dwExitCode = 0;
			unsigned int nSuccess = 0;
			if( GetExitCodeThread( ((HANDLE_SET *)pArguments)->hCopyThread, &dwExitCode ) )
			{
				if( dwExitCode != 0 )
					nSuccess = 1;
				else
					nSuccess = 0;
			}

			CloseHandle( ((HANDLE_SET *)pArguments)->hCopyThread);
			::SendMessage( ((HANDLE_SET *)pArguments)->hUiHandle, UM_COPYCOMPLETE, (WPARAM)nSuccess , 0 );

			
		}
	}

	_endthreadex( 0 );
	
	return 0;
}

int MakeDirectory(LPTSTR lpsFileName)
{
	CString strFileName = lpsFileName;
	CString strTemp = _T("");
	int nPos = 0;

	if(strFileName.GetAt(strFileName.GetLength()-1) != _T('\\'))
		strFileName += _T("\\");

	while(1)
	{
		nPos = strFileName.Find(_T("\\"), nPos+1);
		if(nPos == -1)
			break;
		strTemp = strFileName.Mid(0, nPos);
		CreateDirectory(strTemp, NULL);
	}

	return 1;
}


unsigned WINAPI OneCopyThreadFunc( void* pArguments )
{
	BOOL bRtn = FALSE;

	LPCON_INFO pContentInfo;
	pContentInfo = ( ( CURCURCON_INFO * )pArguments )->pConInfo;

	LPRATIO_INFO pRatioInfo;
	pRatioInfo = ( ( CURCURCON_INFO * )pArguments )->pRatioInfo;

	ENCTYPE_INFO sEncTypeInfo;
	sEncTypeInfo = ( ( CURCURCON_INFO * )pArguments )->sEncTypeInfo;

	if( (BOOL)(*(pContentInfo->pbCancel)) )
	{
		_endthreadex( bRtn );
		return bRtn;
	}

	if( pContentInfo->bDir )
	{
		bRtn = MakeDirectory(pContentInfo->tzDstContentPath);
		Sleep(59);
	}
	else
	{
		TCHAR tzTemp[1024] = {0,};
		TCHAR tzErrorcode[260] = {0,};
		_stprintf_s( tzErrorcode, 260, _T("File : %s"), pContentInfo->tzOrgContentPath );
		::SendMessage(pContentInfo->hUiHandle, UM_MSGMSG, (WPARAM)tzErrorcode, 0 );

		TCHAR* pwzFilename = PathFindFileName( pContentInfo->tzOrgContentPath );
		TCHAR pwzEncFilePath[MAX_PATH] = {0,};
		_stprintf_s( pwzEncFilePath, MAX_PATH, _T("%s\\%s.MS4"), pContentInfo->tzDstContentPath, pwzFilename );

		int nRtn = -1;
		BOOL bResume = TRUE;

		// 원본 파일 열기
		HANDLE hSourceFile = CreateFile( pContentInfo->tzOrgContentPath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
		if( hSourceFile == INVALID_HANDLE_VALUE )
		{
			AfxMessageBox( _T("원본 파일 열기 실패" ));
			
			_endthreadex( bRtn );
			return bRtn;
		}

		// 대상 파일 생성
		HANDLE hTargetFile = INVALID_HANDLE_VALUE;
		nRtn = TMS4CreateFile( (const TCHAR*)pwzEncFilePath, bResume, &hTargetFile );
		if( nRtn != 0 )
		{
			TCHAR tzErr[1024] = {0,};
			_stprintf(tzErr, _T("대상 파일 생성 실패 Errcode : %d"), nRtn);
			AfxMessageBox(tzErr);
			CloseHandle( hSourceFile );

			_endthreadex( bRtn );
			return bRtn;
		}

		// 원본 파일 크기
		LARGE_INTEGER liFileSize;
		liFileSize.QuadPart = 0;
		::GetFileSizeEx( hSourceFile, &liFileSize );
		
		// 파일 포지션 값
		ULONGLONG ullOffset = 0;

		// 이어받기라면 포지션을 해당 부분으로 이동하자
		if( bResume )
		{
			// 받은 파일의 크기가 현재 포인터가 된다
			if( TMS4GetResumFileSize( hTargetFile, &ullOffset ) != 0 )
			{
				AfxMessageBox( _T("이어받기할 대상을 찾지 못하였거나 파일 사이즈 취득에 실패하였습니다. 변환 실패" ));
				CloseHandle( hSourceFile );
				TMS4CloseHandle( hTargetFile );

				_endthreadex( bRtn );
				return bRtn;
			}

			if( ullOffset > (ULONGLONG)liFileSize.QuadPart )
			{
				AfxMessageBox( _T("이미 완료된 파일이거나 잘못된 대상파일로 이어받기 할 수 없습니다. 변환 실패" ));
				CloseHandle( hSourceFile );
				TMS4CloseHandle( hTargetFile );

				_endthreadex( bRtn );
				return bRtn;
			}

			//읽을 파일 포지션도 이동
			LARGE_INTEGER liCalc;
			liCalc.QuadPart = ullOffset;
			::SetFilePointerEx( hSourceFile, liCalc, 0, FILE_BEGIN );

			pRatioInfo->nCopiedSize += ullOffset;
			::SendMessage(pContentInfo->hUiHandle, UM_PRGRSUPDATE, 0, (LPARAM)pRatioInfo);
		}

		DWORD dwRead, dwWrite =0;
		char szBuf[TMS4_CRYPTO_BUFFER_SIZE] = {0,};	// 버퍼 - 크기는 제한 없음


		while( ReadFile( hSourceFile, szBuf, TMS4_CRYPTO_BUFFER_SIZE, &dwRead, NULL ) )
		{
			if( !dwRead )
				break;

			// 버퍼단위 암호화
			if( TMS4WriteFile( hTargetFile, szBuf, dwRead, &dwWrite ) != 0 )
			{
				AfxMessageBox( _T("변환 실패" ));
				CloseHandle( hSourceFile );
				TMS4CloseHandle( hTargetFile );

				_endthreadex( bRtn );
				return bRtn;
			}

			pRatioInfo->nCopiedSize += dwRead;
			::SendMessage(pContentInfo->hUiHandle, UM_PRGRSUPDATE, 0, (LPARAM)pRatioInfo);
		}


		// 파일 변환이 완료. 암호화 정보를 만들어야함
		// hint 값 설정
		int nHint = TMS4_HINT_SOURCE_FILE_SIZE;
		TCHAR tzMacAddress[1024] = {0,};
		TCHAR tzStreaming[1024] = {0,};

		switch(sEncTypeInfo.nEncType)
		{
		case KEY_TYPE_MAC:
			{
				nHint |= TMS4_HINT_MAC_ADDRESS;

				// 임시 정책정보 만들기 = 프리패키징 파일 생성 완료
				nRtn = TMS4MakeFooterNemo( hTargetFile, 1, _T(""), _T(""), _T(""), _T(""), 0, 0, 0 );
				if( nRtn !=  0 ) 
				{
					TCHAR tzErr[1024] = {0,};
					_stprintf(tzErr, _T("변환 실패 Errcode : %d"), nRtn);
					AfxMessageBox(tzErr);
					CloseHandle( hSourceFile );
					TMS4CloseHandle( hTargetFile );

					_endthreadex( bRtn );
					return 0;
				}

				CloseHandle( hSourceFile );
				TMS4CloseHandle( hTargetFile );



				// **여기서부터는 사용자 PC에서 실행되어야 될 코드입니다.**
				// 다운로드 완료가 되면 사용자 PC의 MAC 정보로 정책정보 재적용
				nRtn = TMS4PrepackLocalRecryptEx((const TCHAR*)pwzEncFilePath,	// 암호화 파일 경로
					nHint,				// 암호화 키 정책 MAC
					_T("userId"),		// 유저아이디 / 31자리
					_T("url"),			// url / 255자리
					_T("watermark"),	// 워터마크 / 31자리
					_T("contentId"),	// 콘텐츠고유아이디 / 31자리 / 고유값 
					365,				// 재생가능 기간 365일
					100,				// 재생 가능 횟수 100회
					4,					// 서비스레벨(워터마크, 캡쳐방지, 프린트 허용)  기본값 : 4
					_T(""),				// 사용안함
					7502);				// 회사코드 / 7502 사용
				if( nRtn !=  0 ) 
				{

					TCHAR tzErr[1024] = {0,};
					_stprintf(tzErr, _T("정책정보 재적용 실패 Errcode : %d"), nRtn);
					AfxMessageBox(tzErr);
					CloseHandle( hSourceFile );
					TMS4CloseHandle( hTargetFile );

					_endthreadex( bRtn );
					return 0;
				}
			}
			break;

		case KEY_TYPE_STREAMING:
			{
				nHint |= TMS4_HINT_STREAMING;

				nRtn = TMS4MakeFooterNemo( hTargetFile, // handle
					nHint,				// 암호화 키 정책 MAC
					_T("userId"),		// 유저아이디 / 31자리
					_T("url"),			// url / 255자리
					_T("watermark"),	// 워터마크 / 31자리
					_T("contentId"),	// 콘텐츠고유아이디 / 31자리 / 고유값 
					365,				// 재생가능 기간 365일
					100,				// 재생 가능 횟수 100회
					4 );				// 서비스레벨(워터마크, 캡쳐방지, 프린트 허용)  기본값 : 4

				if( nRtn !=  0 ) 
				{

					TCHAR tzErr[1024] = {0,};
					_stprintf(tzErr, _T("변환 실패 Errcode : %d"), nRtn);
					AfxMessageBox(tzErr);
					::SendMessage(pContentInfo->hUiHandle, UM_MSGMSG, (WPARAM)tzErr, 0 );

					CloseHandle( hSourceFile );
					TMS4CloseHandle( hTargetFile );

					_endthreadex( bRtn );
					return 0;
				}

				CloseHandle( hSourceFile );
				TMS4CloseHandle( hTargetFile );
			}
			break;
		}

	}

	_endthreadex( bRtn );
	return bRtn;
}

unsigned WINAPI CopyThreadFunc( void* pArguments )
{
	RATIO_INFO CopiedRatio;
	ZeroMemory( &CopiedRatio, sizeof(RATIO_INFO) );

	CPrePackagerDlg *pThis = (CPrePackagerDlg*) pArguments;
	
	for ( int nLoop = 0; nLoop < pThis->ArrDnFilesEntry.GetSize(); nLoop++ )
	{
		CON_INFO *oneConFile = (CON_INFO *)(pThis->ArrDnFilesEntry.GetAt( nLoop ));
		CopiedRatio.nFullSize += oneConFile->n64FileSize;
		::SendMessage( oneConFile->hUiHandle, UM_PRGRSSET, 0 , (LPARAM)&CopiedRatio );
	}

	for ( int nLoop = 0; nLoop < pThis->ArrDnFilesEntry.GetSize(); nLoop++ )
	{
		CON_INFO *oneConFile = (CON_INFO *)(pThis->ArrDnFilesEntry.GetAt( nLoop ));

		ENCTYPE_INFO encTypeInfo;
		encTypeInfo.nEncType = pThis->m_valEncType;

		CURCURCON_INFO CurConInfoRef;
		CurConInfoRef.pConInfo = oneConFile;
		CurConInfoRef.pRatioInfo = &CopiedRatio;
		CurConInfoRef.sEncTypeInfo = encTypeInfo;
		
		if( (BOOL)(*(oneConFile->pbCancel)) )
		{
			_endthreadex( -1 );
			return -1;
		}
		

		HANDLE hThread = NULL;
		unsigned threadID = 0;
		DWORD dwExitCode = 0;
		hThread = (HANDLE)_beginthreadex( NULL, 0, &OneCopyThreadFunc, &CurConInfoRef, 0, &threadID );
		if( hThread == NULL )
		{
			_endthreadex( -2 );
			return -2;
		}


		WaitForSingleObject( hThread, INFINITE );

		CloseHandle (hThread);
	}

	_endthreadex( 0 );
	return 0;
}

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CPrePackagerDlg 대화 상자




CPrePackagerDlg::CPrePackagerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrePackagerDlg::IDD, pParent)
	, m_valOriginFolderPath(_T(""))
	, m_valEncFolderPath(_T(""))
	, m_n64OrgConSize(0)
	, m_bEncryptCancel(false)
	//, m_valEncUnitSelRadio(1) // 원본
	, m_valEncUnitSelRadio(0) // 네모닥 수정코드
	//, m_valEncType(KEY_TYPE_MAC) // 원본
	,m_valEncType(KEY_TYPE_STREAMING) // 네모닥 수정코드
	,m_valEncKeyTypeRadio(1) // 네모닥 수정코드
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPrePackagerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_ENCLOG_LIST, m_ctrLogListBox);
	DDX_Text(pDX, IDC_ORIGINFOLDER_EDIT, m_valOriginFolderPath);
	DDX_Text(pDX, IDC_ENCFOLDER_EDIT, m_valEncFolderPath);
	DDX_Control(pDX, IDC_ENCSTATS_PROGRESS, m_ctrEncryptProgressBar);
	DDX_Radio(pDX, IDC_FILESEL_RADIO, m_valEncUnitSelRadio);
	//DDX_Radio(pDX, IDC_KEYTYPE_MAC, m_valEncKeyTypeRadio); 원본
	DDX_Radio(pDX, IDC_KEYTYPE_MAC, m_valEncKeyTypeRadio);
}

BEGIN_MESSAGE_MAP(CPrePackagerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(UM_COPYCOMPLETE, OnCopyComplete)
	ON_MESSAGE(UM_MSGMSG, OnCopyMsgMsg)
	ON_MESSAGE(UM_PRGRSUPDATE, OnPrgrsUpdate)
	ON_MESSAGE(UM_PRGRSSET, OnPrgrsSet)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_ENCSTART_BUTTON, &CPrePackagerDlg::OnBnClickedEncstartButton)
	ON_BN_CLICKED(IDC_ENCEND_BUTTON, &CPrePackagerDlg::OnBnClickedEncendButton)
	ON_BN_CLICKED(IDC_ORIGINSEL_BUTTON, &CPrePackagerDlg::OnBnClickedOriginselButton)
	ON_BN_CLICKED(IDC_ENCSEL_BUTTON, &CPrePackagerDlg::OnBnClickedEncselButton)
	ON_WM_DESTROY()

	ON_BN_CLICKED(IDC_FILESEL_RADIO, &CPrePackagerDlg::OnBnClickedFileselRadio)
	ON_BN_CLICKED(IDC_FOLDERSEL_RADIO, &CPrePackagerDlg::OnBnClickedFolderselRadio)

	ON_BN_CLICKED(IDC_KEYTYPE_MAC, &CPrePackagerDlg::OnBtnClickedMacRadio)
	ON_BN_CLICKED(IDC_KEYTYPE_STREAMING, &CPrePackagerDlg::OnBtnClickedStreamingRadio)
	ON_BN_CLICKED(IDC_DOWNLOAD_BUTTON, &CPrePackagerDlg::OnBnClickedDownloadButton)
	ON_BN_CLICKED(IDC_UPLOAD_BUTTON, &CPrePackagerDlg::OnBnClickedUploadButton)
	ON_BN_CLICKED(IDC_MULTIUPLOAD_BUTTON, &CPrePackagerDlg::OnBnClickedMultiUploadButton)
//	ON_BN_CLICKED(IDC_PERIODIC_RUN, &CPrePackagerDlg::OnBnClickedPeriodicRun)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_COGNITO_RUN, &CPrePackagerDlg::OnBnClickedCognitoRun)
	ON_BN_CLICKED(IDC_FETCHLIST_BUTTON, &CPrePackagerDlg::OnBnClickedFetchlistButton)
	ON_BN_CLICKED(IDC_MULTIDOWNLOAD_BUTTON, &CPrePackagerDlg::OnBnClickedMultiDownloadButton)

END_MESSAGE_MAP()


// CPrePackagerDlg 메시지 처리기

BOOL CPrePackagerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.


	// DRM 라이브러리 로드
	int nRtn = 0;
	nRtn = LoadMS4XLib();
	if( nRtn != 0 )
	{
		AfxMessageBox(_T("라이브러리 로드를 실패했습니다."));
		return FALSE;
	}

	((CButton*)GetDlgItem(IDC_KEYTYPE_STREAMING))->SetCheck(TRUE);
	GetDlgItem(IDC_ENCSTART_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_ENCEND_BUTTON)->EnableWindow(FALSE);
	UpdateData(FALSE);

	//log init
	m_logOut.open("log.txt", std::ofstream::out | std::ofstream::app);

	if (!m_logOut.is_open()) {
		cout << currentDateTime << "[Error] Log file open error\n";
		AfxMessageBox(_T("Log file open error"));
	}


	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

int CPrePackagerDlg::LoadMS4XLib(void)
{
	TCHAR tzTcwLibPath[MAX_PATH] = {0,};
	TCHAR tzCurModuleDrv[_MAX_DRIVE] = {0,};
	TCHAR tzCurModuleDir[_MAX_DIR] = {0,};
	GetModuleFileName(AfxGetInstanceHandle(),tzTcwLibPath,MAX_PATH);
	_tsplitpath_s( tzTcwLibPath,tzCurModuleDrv,_MAX_DRIVE,tzCurModuleDir,_MAX_DIR,NULL,0,NULL,0 );
	_tmakepath_s( tzTcwLibPath, MAX_PATH, tzCurModuleDrv, tzCurModuleDir, _T("TMS4FIOXLIB"), _T("dll") );

	m_hTMS4Xlib = LoadLibrary( tzTcwLibPath );
	if( NULL == m_hTMS4Xlib )
		return -1;
	
	if(m_hTMS4Xlib)
	{
		TMS4XFinal = ( pfnTMS4XFinalize )GetProcAddress( m_hTMS4Xlib, "TMS4EFinalize" );
#ifdef _UNICODE
		TMS4XInit = ( pfnTMS4XInitInstance )GetProcAddress( m_hTMS4Xlib, "TMS4EInitInstanceW" );
		TMS4XFileEncryptEx =  ( pfnTMS4XFileEncryptEx )GetProcAddress( m_hTMS4Xlib, "TMS4EFileEncryptExW" );
		TMS4XFileDecryptEx =  ( pfnTMS4XFileDecryptEx )GetProcAddress( m_hTMS4Xlib, "TMS4EFileDecryptExW" );
		TMS4XFileDecrypt =  ( pfnTMS4XFileDecrypt )GetProcAddress( m_hTMS4Xlib, "TMS4EFileDecryptW" );
		TMS4XFileEncrypt =  ( pfnTMS4XFileEncrypt )GetProcAddress( m_hTMS4Xlib, "TMS4EFileEncryptComW" );
		TMS4XGetHDDSerialNumber = ( pfnTMS4XGetHDDSerialNumber )GetProcAddress( m_hTMS4Xlib, "TMS4EGetHDDSerialNumberW" );
		TMS4XGetMacAddress = ( pfnTMS4XGetMacAddress )GetProcAddress( m_hTMS4Xlib, "TMS4EGetMacAddressW" );
		TMS4XPrepackLocalRecrypt = ( pfnTMS4XPrePackLocalRecrypt )GetProcAddress( m_hTMS4Xlib, "TMS4EPrepackLocalRecryptW" );
		TMS4CreateFile =  ( pfnTMS4ECreateFile )GetProcAddress( m_hTMS4Xlib, "TMS4ECreateFileW" );
		TMS4WriteFile = ( pfnTMS4EWriteFile )GetProcAddress( m_hTMS4Xlib, "TMS4EWriteFile" );
		TMS4MakeFooterNemo = ( pfnTMS4EMakeFooterNemo )GetProcAddress( m_hTMS4Xlib, "TMS4EMakeFooterNemoW" );
		TMS4CloseHandle = ( pfnTMS4ECloseHandle )GetProcAddress( m_hTMS4Xlib, "TMS4ECloseHandle" );
		TMS4GetResumFileSize = ( pfnTMS4EGetResumeFileSize )GetProcAddress( m_hTMS4Xlib, "TMS4EGetResumeFileSize" );
		TMS4GetMacAddress = ( pfnTMS4EGetMacAddress )GetProcAddress( m_hTMS4Xlib, "TMS4EGetMacAddressW" );
		TMS4PrepackLocalRecryptEx = ( pfnTMS4EPrePackLocalRecryptEx )GetProcAddress( m_hTMS4Xlib, "TMS4EPrepackLocalRecryptW" );
		
#else
		TMS4XInit = ( pfnTMS4XInitInstance )GetProcAddress( m_hTMS4Xlib, "TMS4EInitInstanceA" );
		TMS4XFileEncryptEx =  ( pfnTMS4XFileEncryptEx )GetProcAddress( m_hTMS4Xlib, "TMS4EFileEncryptExA" );
		TMS4XFileDecryptEx =  ( pfnTMS4XFileDecryptEx )GetProcAddress( m_hTMS4Xlib, "TMS4EFileDecryptExA" );
		TMS4XFileDecrypt =  ( pfnTMS4XFileDecrypt )GetProcAddress( m_hTMS4Xlib, "TMS4EFileDecryptA" );
		TMS4XFileEncrypt =  ( pfnTMS4XFileEncrypt )GetProcAddress( m_hTMS4Xlib, "TMS4EFileEncryptComA" );
		TMS4XGetHDDSerialNumber = ( pfnTMS4XGetHDDSerialNumber )GetProcAddress( m_hTMS4Xlib, "TMS4EGetHDDSerialNumberA" );
		TMS4XGetMacAddress = ( pfnTMS4XGetMacAddress )GetProcAddress( m_hTMS4Xlib, "TMS4EGetMacAddressA" );
		TMS4XPrepackLocalRecrypt = ( pfnTMS4XPrePackLocalRecrypt )GetProcAddress( m_hTMS4Xlib, "TMS4EPrepackLocalRecryptA" );
		TMS4CreateFile =  ( pfnTMS4ECreateFile )GetProcAddress( m_hTMS4Xlib, "TMS4ECreateFileA" );
		TMS4WriteFile = ( pfnTMS4EWriteFile )GetProcAddress( m_hTMS4Xlib, "TMS4EWriteFile" );
		TMS4MakeFooterNemo = ( pfnTMS4EMakeFooterNemo )GetProcAddress( m_hTMS4Xlib, "TMS4EMakeFooterNemoA" );
		TMS4CloseHandle = ( pfnTMS4ECloseHandle )GetProcAddress( m_hTMS4Xlib, "TMS4ECloseHandle" );
		TMS4GetResumFileSize = ( pfnTMS4EGetResumeFileSize )GetProcAddress( m_hTMS4Xlib, "TMS4EGetResumeFileSize" );
		TMS4GetMacAddress = ( pfnTMS4EGetMacAddress )GetProcAddress( m_hTMS4Xlib, "TMS4EGetMacAddressA" );
		TMS4PrepackLocalRecryptEx = ( pfnTMS4EPrePackLocalRecryptEx )GetProcAddress( m_hTMS4Xlib, "TMS4EPrepackLocalRecryptA" );
#endif
	}

	if ( TMS4XInit == NULL || TMS4XFinal == NULL || TMS4XGetMacAddress == NULL || TMS4XGetHDDSerialNumber == NULL || TMS4XFileEncryptEx == NULL || TMS4XFileDecryptEx == NULL || TMS4XFileEncrypt == NULL  || TMS4XFileDecrypt == NULL || TMS4XPrepackLocalRecrypt == NULL )
	{
		FreeLibrary( m_hTMS4Xlib );
		m_hTMS4Xlib = NULL;

		return -2;
	}

	return 0;
}

void CPrePackagerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CPrePackagerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CPrePackagerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CPrePackagerDlg::OnBnClickedEncstartButton()
{
	UpdateData( TRUE );


	if( m_valOriginFolderPath.IsEmpty() )
	{
		AfxMessageBox( _T( "원본 폴더 경로가 없습니다. 경로를 확인하세요." ) );
		return;
	}

	if( m_valEncFolderPath.IsEmpty() )
	{
		AfxMessageBox( _T( "저장할 폴더 경로가 없습니다. 경로를 확인하세요." ) );
		return;
	}

	//m_ctrLogListBox.ResetContent();
	m_ctrLogListBox.AddString(_T("Enc Start Command"));
	m_bEncryptCancel = FALSE;

	//암호화 쓰레드 시작
	
	GetDlgItem(IDC_ENCSTART_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_ENCEND_BUTTON)->EnableWindow(TRUE);

	
	EncryptProcess();
}

ULONGLONG CPrePackagerDlg::GetFileSize(TCHAR* tzFilePath)
{
	HANDLE hFile = CreateFile(tzFilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);

	LARGE_INTEGER liFileSize;
	liFileSize.QuadPart = 0;
	::GetFileSizeEx( hFile, &liFileSize );

	CloseHandle(hFile);

	return liFileSize.QuadPart;
}

int CPrePackagerDlg::EncFileListClear(void)
{
	for(int nLoop = 0; ArrDnFilesEntry.GetSize() > nLoop; nLoop++)
	{
		CON_INFO *oneConInfo = ((CON_INFO*)ArrDnFilesEntry.GetAt(nLoop));
		delete oneConInfo;
	}
	ArrDnFilesEntry.RemoveAll();
	ArrDnFilesEntry.FreeExtra();

	m_n64OrgConSize = 0;

	return 0;
}

void CPrePackagerDlg::EncOneFileListing()
{
	HWND hUiWnd = NULL;
	hUiWnd = GetSafeHwnd();
	TCHAR tzErrorcode[260] = {0,};

	CON_INFO *oneDnInfo = new CON_INFO;
	ZeroMemory( oneDnInfo, sizeof( CON_INFO ) );

	_tcscpy_s( oneDnInfo->tzOrgContentPath, MAX_PATH, m_valOriginFolderPath.GetBuffer(0) );		
	_stprintf_s( oneDnInfo->tzDstContentPath, MAX_PATH, _T("%s"),m_valEncFolderPath.GetBuffer(0) );

	m_n64OrgConSize += GetFileSize(oneDnInfo->tzOrgContentPath);

	oneDnInfo->n64FileSize = m_n64OrgConSize;
	oneDnInfo->hUiHandle = hUiWnd;
	oneDnInfo->pbCancel = &m_bEncryptCancel;

	_stprintf_s( tzErrorcode, 260, _T("Add File: %s, %s"), oneDnInfo->tzOrgContentPath, oneDnInfo->tzDstContentPath );
	::SendMessage(hUiWnd, UM_MSGMSG, (WPARAM)tzErrorcode, 0 );

	ArrDnFilesEntry.Add( oneDnInfo );
}

void CPrePackagerDlg::EncFileListing(CString strDirPath, int baseDirLenth)
{
	HWND hUiWnd = NULL;
	hUiWnd = GetSafeHwnd();

	CFileFind finder;
	CString strWildcard = strDirPath;

	if ( strWildcard.Right(1) != _T('\\') )
		strWildcard += _T('\\');

	strWildcard += _T("*.*");
	
	BOOL bWorking = finder.FindFile(strWildcard);
	while (bWorking)
	{
		bWorking = finder.FindNextFile();
		if (finder.IsDots())
			continue;

		if (finder.MatchesMask(FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_SYSTEM))
			continue;
		
		CON_INFO *oneDnInfo = new CON_INFO;
		ZeroMemory( oneDnInfo, sizeof( CON_INFO ) );

		
		TCHAR tzErrorcode[260] = {0,};

		if (finder.IsDirectory())
		{
			oneDnInfo->bDir = TRUE;

			CString strTempTemp = _T("");
			strTempTemp = finder.GetFilePath();
			strTempTemp.Replace(m_valOriginFolderPath,_T(""));

			_stprintf_s( oneDnInfo->tzDstContentPath, MAX_PATH, _T("%s%s"),m_valEncFolderPath.GetBuffer(0), strTempTemp );
			oneDnInfo->hUiHandle = hUiWnd;
			oneDnInfo->pbCancel = &m_bEncryptCancel;

			_stprintf_s( tzErrorcode, 260, _T("Add Folder : %s"), oneDnInfo->tzDstContentPath );
			::SendMessage(hUiWnd, UM_MSGMSG, (WPARAM)tzErrorcode, 0 );

			ArrDnFilesEntry.Add( oneDnInfo );

			CString str = finder.GetFilePath();
			EncFileListing(str, baseDirLenth);
		}
		else
		{
			m_n64OrgConSize += finder.GetLength();

			_tcscpy_s( oneDnInfo->tzOrgContentPath, MAX_PATH, finder.GetFilePath().GetBuffer(0) );
			_tcscpy_s( oneDnInfo->tzOrgContentName, MAX_PATH, finder.GetFileName().GetBuffer(0) );
			
			CString strTempTemp = _T("");
			strTempTemp = finder.GetFilePath();
			strTempTemp.Replace(m_valOriginFolderPath,_T(""));
			_stprintf_s( oneDnInfo->tzDstContentPath, MAX_PATH, _T("%s%s"),m_valEncFolderPath.GetBuffer(0), strTempTemp );

			PathRemoveFileSpec(oneDnInfo->tzDstContentPath);
			oneDnInfo->n64FileSize = GetFileSize(oneDnInfo->tzOrgContentPath);
			oneDnInfo->hUiHandle = hUiWnd;
			oneDnInfo->pbCancel = &m_bEncryptCancel;

			_stprintf_s( tzErrorcode, 260, _T("Add File: %s, %s"), oneDnInfo->tzOrgContentPath, oneDnInfo->tzDstContentPath );
			::SendMessage(hUiWnd, UM_MSGMSG, (WPARAM)tzErrorcode, 0 );

			ArrDnFilesEntry.Add( oneDnInfo );
		}
	}
	finder.Close();
}


int CPrePackagerDlg::EncryptProcess(void)
{
	EncFileListClear();

	if( m_valEncUnitSelRadio == 1 )
		EncFileListing( m_valOriginFolderPath, m_valEncFolderPath.GetLength() );
	else
		EncOneFileListing();


	int nCount = ArrDnFilesEntry.GetSize();
	if( ArrDnFilesEntry.GetSize() == 0 )
	{
		AfxMessageBox(_T("컨텐츠 파일이 없습니다.\n컨텐츠가 있는 폴더를 선택해 주십시오."));
		GetDlgItem(IDC_ENCSTART_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_ENCEND_BUTTON)->EnableWindow(FALSE);
		return -1;
	}

	TCHAR tzDrvPath[10] = {0,};
	_tsplitpath_s( m_valEncFolderPath.GetBuffer(0), tzDrvPath, 10, NULL, 0, NULL, 0, NULL, 0 );

	ULARGE_INTEGER availableToCaller,totalByte,freeByte;
	GetDiskFreeSpaceEx( tzDrvPath, &availableToCaller, &totalByte, &freeByte);

	if( m_n64OrgConSize > availableToCaller.QuadPart )
	{
		AfxMessageBox(_T("컨텐츠의 크기가 저장공간 보다 큽니다."));
		GetDlgItem(IDC_ENCSTART_BUTTON)->EnableWindow(TRUE);
		GetDlgItem(IDC_ENCEND_BUTTON)->EnableWindow(FALSE);
		return -1;
	}

	OriginToEnc();

	return 0;
}

int CPrePackagerDlg::OriginToEnc(void)
{
	HANDLE hThread = NULL;
	HANDLE hWaitThread = NULL;
	unsigned threadID = 0;
	unsigned WaitThreadID = 0;
	m_TwoHandleSet.hUiHandle = GetSafeHwnd();

	// DRM 모듈 초기화
	int nRtn = 0;
	if( (nRtn = TMS4XInit(_T("SellerID"))) != 0)
	{
		TCHAR tzError[1024] = {0,};
		_stprintf(tzError, _T("TMS4XInit Error : %d"), nRtn);
		AfxMessageBox(tzError);
		return -1;
	}

	hThread = (HANDLE)_beginthreadex( NULL, 0, &CopyThreadFunc, this, 0, &threadID );
	if( hThread == NULL )
		return -1;

	m_TwoHandleSet.hCopyThread = hThread;

	hWaitThread = (HANDLE)_beginthreadex( NULL, 0, &WaitCopyThreadFunc, &m_TwoHandleSet, 0, &WaitThreadID );
	//hWaitThread 함수를 가보면 hThread 가 끝나기를 wait하고 있어 중복으로 기다릴 필요가 없는듯하다.
	//WaitForSingleObject(hThread, INFINITE);
	if( hWaitThread == NULL )
	{
		CloseHandle ( hThread );
		return -1;
	}
	//nemolab 추가 encrypt 작업이 끝나기 전에 multiupload가 시작되는것 같아서 추가함 => 그러나 이것으로 인해 encrypt 스레드가 영원히 끝나지 않는것 같음
	WaitForSingleObject(hWaitThread, INFINITE);

	CloseHandle (hWaitThread);

	return 0;
	
}

void CPrePackagerDlg::OnBnClickedEncendButton()
{
	m_ctrLogListBox.AddString(_T("Enc Stop Command"));
	m_bEncryptCancel = TRUE;
	//암호화 쓰레드 중지
}

void CPrePackagerDlg::OnBnClickedOriginselButton()
{
	if(m_valEncUnitSelRadio == 1 )
	{
		TCHAR tzFolder[MAX_PATH] = {0,};

		ITEMIDLIST *pidlBrowse;

		BROWSEINFO BrInfo;
		memset(&BrInfo, 0, sizeof(BrInfo));

		BrInfo.hwndOwner = GetSafeHwnd(); 
		BrInfo.pidlRoot = NULL;

		BrInfo.pszDisplayName = tzFolder;
		BrInfo.lpszTitle =_T("Select Path");
		BrInfo.ulFlags = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;
		BrInfo.lParam = NULL;

		pidlBrowse = ::SHBrowseForFolder(&BrInfo);

		if( pidlBrowse == NULL)
			return;

		::SHGetPathFromIDList(pidlBrowse, tzFolder);
		m_valOriginFolderPath = tzFolder;
		if ( m_valOriginFolderPath.Right(1) != _T('\\') )
			m_valOriginFolderPath += _T('\\');
	}
	else
	{
		TCHAR tzPath[MAX_PATH] = {0};

		OPENFILENAME ofn={0};
		ofn.lStructSize = sizeof(OPENFILENAME);
		ofn.hwndOwner = GetSafeHwnd();
		ofn.lpstrFilter = _T("All Files (*.*)\0*.*\0");
		ofn.lpstrCustomFilter = NULL;
		ofn.nFilterIndex = 1;
		ofn.lpstrFile = tzPath;
		ofn.nMaxFile = 1024;
		ofn.lpstrTitle = _T("암호화 할 파일 고르기\0");
		ofn.lpstrFileTitle = NULL;
		ofn.lpstrDefExt = TEXT("*\0");
		ofn.Flags = OFN_FILEMUSTEXIST | OFN_READONLY | OFN_PATHMUSTEXIST | OFN_EXPLORER;
		ofn.lpstrInitialDir = NULL;

		if ( !GetOpenFileName( (LPOPENFILENAME)&ofn ) )
			return;

		m_valOriginFolderPath = tzPath;
		
		
	}

	UpdateData(FALSE);
}

void CPrePackagerDlg::OnBnClickedEncselButton()
{
	TCHAR tzFolder[MAX_PATH] = {0,};

	ITEMIDLIST *pidlBrowse;

	BROWSEINFO BrInfo;
	memset(&BrInfo, 0, sizeof(BrInfo));

	BrInfo.hwndOwner = GetSafeHwnd(); 
	BrInfo.pidlRoot = NULL;

	BrInfo.pszDisplayName = tzFolder;
	BrInfo.lpszTitle =_T("Select Path");
	BrInfo.ulFlags = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;
	BrInfo.lParam = NULL;

	pidlBrowse = ::SHBrowseForFolder(&BrInfo);

	if( pidlBrowse == NULL)
		return;

	::SHGetPathFromIDList(pidlBrowse, tzFolder);
	m_valEncFolderPath = tzFolder;
	if ( m_valEncFolderPath.Right(1) != _T('\\') )
		m_valEncFolderPath += _T('\\');

	UpdateData(FALSE);
}

LRESULT CPrePackagerDlg::OnCopyComplete(WPARAM wp, LPARAM lp)
{
	if( (int)wp )
		m_ctrLogListBox.AddString(_T("Enc Stop"));
	else
		m_ctrLogListBox.AddString(_T("Enc Complete"));

	GetDlgItem(IDC_ENCSTART_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_ENCEND_BUTTON)->EnableWindow(FALSE);

	
	//AfxMessageBox(_T("암호화 과정 완료")); 테르텐 원본 코드

	// DRM 모듈 해제 , 암호화 완료 이후 또는 프로그램 종료시 호출
	TMS4XFinal();
	
	MultiUpload();
	return 0;
}


LRESULT CPrePackagerDlg::OnCopyMsgMsg(WPARAM wp, LPARAM lp)
{
	m_ctrLogListBox.AddString((TCHAR *)wp);

	return 0;
}

LRESULT CPrePackagerDlg::OnPrgrsUpdate(WPARAM wParam, LPARAM lParam)
{
	RATIO_INFO* CopiedRatio = (RATIO_INFO*)lParam;

	m_ctrEncryptProgressBar.SetPos(CopiedRatio->nCopiedSize / 1000);
	m_ctrEncryptProgressBar.UpdateData();

	if (CopiedRatio->nCopiedSize >= CopiedRatio->nFullSize) {
		//cout << currentDateTime() << "Encrypt Progress Updating... \n";
		CPrePackagerDlg::Logger(currentDateTime() + "Encrypt Progress Updating... \n");
	}

	return 0;
}

LRESULT CPrePackagerDlg::OnPrgrsSet(WPARAM wParam, LPARAM lParam)
{
	RATIO_INFO* CopiedRatio = (RATIO_INFO*)lParam;

	m_ctrEncryptProgressBar.SetRange32(0, CopiedRatio->nFullSize / 1000);
	m_ctrEncryptProgressBar.SetPos(0);
	m_ctrEncryptProgressBar.UpdateWindow();

	return 0;
}

void CPrePackagerDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	EncFileListClear();
}

void CPrePackagerDlg::OnBnClickedFileselRadio()
{
	int nOldVal = m_valEncUnitSelRadio;
	UpdateData();

	if( nOldVal != m_valEncUnitSelRadio )
	{
		m_valOriginFolderPath = _T("");
		UpdateData(0);
	}
}

void CPrePackagerDlg::OnBnClickedFolderselRadio()
{
	int nOldVal = m_valEncUnitSelRadio;
	UpdateData();

	if( nOldVal != m_valEncUnitSelRadio )
	{
		m_valOriginFolderPath = _T("");
		UpdateData(0);
	}
}


void CPrePackagerDlg::OnBtnClickedMacRadio()
{
	int nOldVal = m_valEncKeyTypeRadio;
	UpdateData();

	if( nOldVal != m_valEncKeyTypeRadio )
	{
		m_valEncType = KEY_TYPE_MAC;
		UpdateData(0);
	}
}

void CPrePackagerDlg::OnBtnClickedStreamingRadio()
{
	int nOldVal = m_valEncKeyTypeRadio;
	UpdateData();

	if( nOldVal != m_valEncKeyTypeRadio )
	{
		m_valEncType = KEY_TYPE_STREAMING;
		UpdateData(0);
	}
}


// 웹상의 파일을 다운로드
int CPrePackagerDlg::getFileFromHttp(LPCWCHAR pszUrl, char* pszFile)
{
	HINTERNET    hInet, hUrl;
	DWORD        dwReadSize = 0;

	// WinINet함수 초기화
	if ((hInet = InternetOpen(_T("MyWeb"),            // user agent in the HTTP protocol
		INTERNET_OPEN_TYPE_DIRECT,    // AccessType
		NULL,                        // ProxyName
		NULL,                        // ProxyBypass
		0)) != NULL)                // Options
	{

		// 입력된 HTTP주소를 열기
		if ((hUrl = InternetOpenUrl(hInet,        // 인터넷 세션의 핸들
			pszUrl,                        // URL
			NULL,                        // HTTP server에 보내는 해더
			0,                            // 해더 사이즈
			INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_NO_COOKIES, // no cache Flag
			0)) != NULL)                // Context
		{
			// 다운로드할 파일 만들기
			FILE    *fp;
			if ((fp = fopen((char *)pszFile, "wb")) != NULL)
			{
				TCHAR    szBuff[READ_BUF_SIZE];
				DWORD    dwSize;
				DWORD    dwDebug = 10;

				do {
					// 웹상의 파일 읽기
					InternetReadFile(hUrl, szBuff, READ_BUF_SIZE, &dwSize);
					
					// 읽은 파일의 내용을 콘솔에 출력
					//fwrite(szBuff, dwSize, 1, stdout);
					
					// 웹상의 파일을 만들어진 파일에 써넣기
					fwrite(szBuff, dwSize, 1, fp);
					
					dwReadSize += dwSize;
				} while ((dwSize != 0) || (--dwDebug != 0));
				fclose(fp);
				
				
				char charbuf[128];
				sprintf_s(charbuf, "%s is downloaded", pszFile);
				m_ctrLogListBox.ResetContent();
				m_ctrLogListBox.AddString((CA2W) charbuf);
				
			}
			// 인터넷 핸들 닫기
			InternetCloseHandle(hUrl);
		}
		// 인터넷 핸들 닫기
		InternetCloseHandle(hInet);
	}
	else {
		ErrorExit(TEXT("InternetOpen"));
	}
	

	return(dwReadSize);
}


void CPrePackagerDlg::ErrorExit(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message and exit the process

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		lpszFunction, dw, lpMsgBuf);
	MessageBox((LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);
	

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
	ExitProcess(dw);
}


void CPrePackagerDlg::OnBnClickedDownloadButton()
{
	CWinThread *p1 = NULL;
	p1 = AfxBeginThread(downloadThreadFunc, this);

	if (p1 == NULL)
	{
		AfxMessageBox(L"Error");
	}
	
	CloseHandle(p1->m_hThread); 
	p1->m_hThread = NULL;
}

UINT CPrePackagerDlg::downloadThreadFunc(LPVOID _mothod) {

	CPrePackagerDlg *hThis = (CPrePackagerDlg*)_mothod;
	Aws::SDKOptions options;
	options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Trace;
	Aws::InitAPI(options);
	{
		// Download from s3 using GetObject
		
		char *bucket_name = "for.win.batch.job.test";
		std::string key_name = "SampleVideo_1280x720_30mb.mp4";

		Aws::Client::ClientConfiguration clientConfig;
		clientConfig.region = "ap-northeast-2";

		//Aws::S3::S3Client s3_client;
		std::unique_ptr< Aws::S3::S3Client > s3_client(new Aws::S3::S3Client(clientConfig));
		Aws::S3::Model::GetObjectRequest object_request;
		object_request.WithBucket(bucket_name).WithKey(key_name.c_str());

		auto get_object_outcome = s3_client.get()->GetObject(object_request);
		//auto get_object_outcome = s3_client.GetObject(object_request);

		if (get_object_outcome.IsSuccess())
		{
			Aws::OFStream local_file;
			std::string strFileName = "download_" + key_name;
			CString cstrFileName(strFileName.c_str());

			local_file.open(cstrFileName, std::ios::out | std::ios::binary);
			local_file << get_object_outcome.GetResult().GetBody().rdbuf();
			//std::cout <<  currentDateTime() << "download is done" << std::endl;
			hThis->Logger(currentDateTime() + "download is done\n");
		}
		else
		{
			//std::cout <<  currentDateTime() << "s3 download error: " <<
			//	get_object_outcome.GetError().GetExceptionName() << " " <<
			//	get_object_outcome.GetError().GetMessage() << std::endl;
			hThis->Logger(currentDateTime() + "s3 download error: " +
				get_object_outcome.GetError().GetExceptionName() + " " +
				get_object_outcome.GetError().GetMessage() + "\n");
		}
	}
	Aws::ShutdownAPI(options);
	return 0;
}




void CPrePackagerDlg::OnBnClickedMultiDownloadButton()
{

	CWinThread *p1 = NULL;
	p1 = AfxBeginThread(multiDownloadThreadFunc, this);

	if (p1 == NULL)
	{
		AfxMessageBox(L"Error");
	}

	CloseHandle(p1->m_hThread);
	p1->m_hThread = NULL;
}


UINT CPrePackagerDlg::multiDownloadThreadFunc(LPVOID _mothod) {


	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//Multipart download
	CPrePackagerDlg *hThis = (CPrePackagerDlg*)_mothod;

	Aws::SDKOptions options;
	options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Trace;
	Aws::InitAPI(options);
	{

		Aws::String bucket = "for.win.batch.job.test";

		// Set up S3 client
		ClientConfiguration config;
		config.region = "ap-northeast-2";
		config.connectTimeoutMs = 3000;
		config.requestTimeoutMs = 60000;
		//auto s3Client = Aws::MakeShared<S3Client>(ALLOCATION_TAG, config, false);

		auto s3Client = Aws::MakeShared<S3Client>("MULTIPART_DOWNLOAD", config);

		auto sdk_client_executor = Aws::MakeShared<Aws::Utils::Threading::DefaultExecutor>("MULTIPART_DOWNLOAD");
		Aws::Transfer::TransferManagerConfiguration transferConfig(sdk_client_executor.get());
		transferConfig.s3Client = s3Client;
		//transferConfig.maxParallelTransfers = 1;


		std::mutex mtx_lock;

		transferConfig.downloadProgressCallback =
			[&](const Aws::Transfer::TransferManager*, const std::shared_ptr<const Aws::Transfer::TransferHandle>&transferHandle)
		{
			mtx_lock.lock();
			//std::cout <<  currentDateTime() << "Download Progress: " << transferHandle->GetBytesTransferred() << " of " << transferHandle->GetBytesTotalSize() << " bytes\n";
			std::ostringstream osstr;
			osstr << currentDateTime() << "Download Progress: "<< transferHandle->GetBytesTransferred() << " of " << transferHandle->GetBytesTotalSize() << " bytes\n";
			hThis->Logger(osstr.str());
			COORD curPosition = getXY();
			gotoXY(0, curPosition.Y - 1);
			mtx_lock.unlock();
		};

		
		std::shared_ptr<Aws::Transfer::TransferManager> transferManager = Aws::Transfer::TransferManager::Create(transferConfig);


		//Aws::String fileToUpload = "Venom.2018.720p.WEBRip.x264.AAC2.0-SHITBOX-002.mp4";
		Aws::String fileToDownload = "SampleVideo_1280x720_30mb.mp4";
		std::shared_ptr<Aws::Transfer::TransferHandle> transferHandle = transferManager->DownloadFile(bucket.c_str(), fileToDownload.c_str(), "downloaded.mp4");


		size_t retries = 0;
		transferHandle->WaitUntilFinished();
		while (transferHandle->GetStatus() == Aws::Transfer::TransferStatus::FAILED && retries++ < 5)
		{
			transferManager->RetryDownload(transferHandle);
			transferHandle->WaitUntilFinished();
		}
		transferHandle->SetIsMultipart(true);

		Aws::Transfer::PartStateMap completedParts = transferHandle->GetCompletedParts();
		

		for (const auto &p : completedParts)
		{
			//std::cout << "completed download parts[" << p.first << "]" << p.second->GetETag() << '\n';
			std::ostringstream osstr; 
			osstr << "completed download parts[" << p.first << "]" << p.second->GetETag() << '\n';
			hThis->Logger(osstr.str());
		}

		Aws::Transfer::TransferStatus status = transferHandle->GetStatus();
		std::string res("Result:");
		res.append(std::to_string((int)status));

		//std::cout << res.c_str() << std::endl;
		std::ostringstream osstr;
		osstr << res.c_str() << "\n";
		hThis->Logger(osstr.str());
		

	}
	Aws::ShutdownAPI(options);
	return 0;
}



void CPrePackagerDlg::OnBnClickedUploadButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//upload using putobject
	Aws::SDKOptions options;
	options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Trace;
	Aws::InitAPI(options);
	{
		Aws::Client::ClientConfiguration clientConfig;
		clientConfig.region = "ap-northeast-2";

		Aws::S3::S3Client *s3_client = new Aws::S3::S3Client(clientConfig);

		char *bucket_name = "for.win.batch.job.test";
		//std::string key_name = "Venom.2018.720p.WEBRip.x264.AAC2.0-SHITBOX-002.mp4";
		std::string key_name = "SampleVideo_1280x720_30mb.mp4";
		
		

		Aws::S3::Model::PutObjectRequest object_request;
		object_request.WithBucket(bucket_name).WithKey(key_name.c_str());

		// Binary files must also have the std::ios_base::bin flag or'ed in
		auto input_data = Aws::MakeShared<Aws::FStream>("PutObjectInputStream",
			key_name.c_str(), std::ios_base::in | std::ios_base::binary);

		object_request.SetBody(input_data);
		//object_request.SetDataSentEventHandler(&UploadprogressCallBack);

		auto put_object_outcome = s3_client->PutObject(object_request);

		if (put_object_outcome.IsSuccess())
		{
			//std::cout <<  currentDateTime() << "Upload is done!" << std::endl;
			CPrePackagerDlg::Logger(currentDateTime() + "Upload is done!\n");
		}
		else
		{
			//std::cout <<  currentDateTime() << "PutObject error: " <<
			//	put_object_outcome.GetError().GetExceptionName() << " " <<
			//	put_object_outcome.GetError().GetMessage() << std::endl;
			CPrePackagerDlg::Logger(currentDateTime() + "PutObject error: " + put_object_outcome.GetError().GetExceptionName() + " " + put_object_outcome.GetError().GetMessage() + "\n");
				
		}
		
	}
	Aws::ShutdownAPI(options);
}


void CPrePackagerDlg::OnBnClickedMultiUploadButton()
{
	Aws::SDKOptions options;
	options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Trace;

	Aws::InitAPI(options);

	Aws::String bucket = "for.win.batch.job.test";

	// Set up S3 client
	ClientConfiguration config;
	config.region = "ap-northeast-2";
	config.connectTimeoutMs = 3000;
	config.requestTimeoutMs = 60000;
	//auto s3Client = Aws::MakeShared<S3Client>(ALLOCATION_TAG, config, false);
	
	auto s3Client = Aws::MakeShared<S3Client>(ALLOCATION_TAG,config);

	auto sdk_client_executor = Aws::MakeShared<Aws::Utils::Threading::DefaultExecutor>(ALLOCATION_TAG);
	Aws::Transfer::TransferManagerConfiguration transferConfig(sdk_client_executor.get());
	transferConfig.s3Client = s3Client;
	//transferConfig.maxParallelTransfers = 1;


	std::mutex mtx_lock;
	
	transferConfig.uploadProgressCallback =
		[&](const Aws::Transfer::TransferManager*, const std::shared_ptr<const Aws::Transfer::TransferHandle>&transferHandle)
	{ 
		mtx_lock.lock();
		//std::cout << currentDateTime() << "Upload Progress: " << transferHandle->GetBytesTransferred() << " of " << transferHandle->GetBytesTotalSize() << " bytes\n";
		std::ostringstream osstr;
		osstr << currentDateTime() << "Upload Progress: " << transferHandle->GetBytesTransferred() << " of " << transferHandle->GetBytesTotalSize() << " bytes\n";
		CPrePackagerDlg::Logger(osstr.str());
		COORD curPosition = getXY();
		gotoXY(0, curPosition.Y-1);
		mtx_lock.unlock();
	};

	
	std::shared_ptr<Aws::Transfer::TransferManager> transferManager = Aws::Transfer::TransferManager::Create(transferConfig);

	//Aws::String fileToUpload = "Venom.2018.720p.WEBRip.x264.AAC2.0-SHITBOX-002.mp4";
	Aws::String fileToUpload = "SampleVideo_1280x720_30mb.mp4";
	std::shared_ptr<Aws::Transfer::TransferHandle> transferHandle = transferManager->UploadFile(fileToUpload.c_str(), bucket.c_str(), fileToUpload.c_str(),
		"multipart/form-data", Aws::Map<Aws::String, Aws::String>());

	
	
	size_t retries = 0;
	transferHandle->WaitUntilFinished();
	while (transferHandle->GetStatus() == Aws::Transfer::TransferStatus::FAILED && retries++ < 5)
	{
		transferManager->RetryUpload(fileToUpload.c_str(), transferHandle);
		transferHandle->WaitUntilFinished();
	}
	transferHandle->SetIsMultipart(true);

	Aws::Transfer::PartStateMap completedParts = transferHandle->GetCompletedParts();
	

	for (const auto &p : completedParts)
	{
		//std::cout << currentDateTime() << "completedparts[" << p.first << "]" << p.second->GetETag() << '\n';
		std::ostringstream osstr;
		osstr << currentDateTime() << "completedparts[" << p.first << "]" << p.second->GetETag() << '\n';
		CPrePackagerDlg::Logger(osstr.str());
	}

	Aws::Transfer::TransferStatus status = transferHandle->GetStatus();
	std::string res("Result:");
	res.append(std::to_string((int)status));

	//std::cout <<  currentDateTime() << res.c_str() << std::endl;
	std::ostringstream osstr;
	osstr << currentDateTime() << res.c_str() << "\n";
	CPrePackagerDlg::Logger(osstr.str());
	

	Aws::ShutdownAPI(options);
}



BOOL CPrePackagerDlg::gotoXY(int x, int y)
{
	COORD pos;   //short 타입의 X, Y 속성이 들어 있는 구조체이다.
	pos.X = x;
	pos.Y = y;
	return SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
}


COORD CPrePackagerDlg::getXY()
{
	COORD pos;
	CONSOLE_SCREEN_BUFFER_INFO buf;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &buf);
	pos.X = buf.dwCursorPosition.X;
	pos.Y = buf.dwCursorPosition.Y;
	return pos;
}



unsigned WINAPI periodicThreadFunc(void* pArguments)
{
	CPrePackagerDlg* hThis = (CPrePackagerDlg*)pArguments;
	// 1. fetch json 
	//cout << CPrePackagerDlg::currentDateTime() << "Fetch new list\n";
	hThis->Logger(CPrePackagerDlg::currentDateTime() + "Fetch new list\n");
	char *fileName = "download_list.json";
	hThis->getFileFromHttp((LPCWCHAR)_T("http://dev.nemodax.com/api/file/selectEncryptTarget.do?key=qpalmfheuzjewgff"), fileName);

	// 2. json file parsing
	// read File
	{
		string strFileName = fileName;
		string sumStr;

		ifstream openFile(strFileName.data());
		if (openFile.is_open()) {
			string line;

			while (getline(openFile, line)) {
				cout << line << "\n";
				hThis->m_logOut << line << "\n";
				sumStr += line;
			}

			openFile.close();
		}

		//json parsing
		Json::Reader reader;
		Json::Value root;
		reader.parse(sumStr, root);


		const Json::Value &targetPath = root["data"]["server_file_path"];
		const Json::Value &saleFileNo = root["data"]["sale_file_no"];
		std::string strTargetPath = targetPath.asString();
		hThis->iSaleFileNo = saleFileNo.asInt();
		
		if (strTargetPath == "" || hThis->iSaleFileNo == 0) {
			//cout <<  CPrePackagerDlg::currentDateTime() << "[info] no more lists \n";
			//cout <<  CPrePackagerDlg::currentDateTime() << "Waiting 60s to fetch the list...\n";
			hThis->Logger(CPrePackagerDlg::currentDateTime() + "[info] no more lists \n");
			hThis->Logger(CPrePackagerDlg::currentDateTime() + "Waiting 60s to fetch the list...\n");
			hThis->runSignal = CPrePackagerDlg::RunSignal::STAT_RUN_WAIT;
			hThis->SetTimer(BATCHJOB_TIMER, 60000, 0); // 1000 = 1초
			return ERR_NO_MORE_LIST;
		}
		//cout << CPrePackagerDlg::currentDateTime() << strTargetPath << endl;
		hThis->Logger(CPrePackagerDlg::currentDateTime() + strTargetPath + "\n");
		hThis->m_strTargetPath = strTargetPath;
	}


	// 3. file download from s3
	{
		string strTargetPath = hThis->m_strTargetPath;
		Aws::SDKOptions options;
		options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Trace;
		Aws::InitAPI(options);
		{
			// Download from s3 using GetObject
			
			char *bucket_name = "nemodax-upload-dev";
			std::string key_name = strTargetPath;

			Aws::Client::ClientConfiguration clientConfig;
			clientConfig.region = "ap-northeast-2";

			//Aws::S3::S3Client s3_client;
			std::unique_ptr< Aws::S3::S3Client > s3_client(new Aws::S3::S3Client(clientConfig));
			Aws::S3::Model::GetObjectRequest object_request;
			object_request.WithBucket(bucket_name).WithKey(key_name.c_str());

			// parse file name from path
			string str_arr[1000];
			int str_cnt = 0;

			char *str_buff = new char[1000];
			strcpy(str_buff, strTargetPath.c_str());

			char *tok = strtok(str_buff, "/");
			while (tok != nullptr) {
				str_arr[str_cnt++] = string(tok);
				tok = strtok(nullptr, "/");
			}

			string fileName = str_arr[str_cnt - 1];

			auto get_object_outcome = s3_client.get()->GetObject(object_request);

			if (get_object_outcome.IsSuccess())
			{
				Aws::OFStream local_file;
				std::string strFileName = fileName;
				hThis->m_origFileNameString = strFileName;
				hThis->m_origFileName = strFileName.c_str();

				// 다운로드된 내용 파일로 저장
				local_file.open(hThis->m_origFileName, std::ios::out | std::ios::binary);
				local_file << get_object_outcome.GetResult().GetBody().rdbuf();
				//std::cout << CPrePackagerDlg::currentDateTime() << "download is done" << std::endl;
				hThis->Logger(CPrePackagerDlg::currentDateTime() + "download is done\n");

				// 다운로드된 원본 파일 경로를 멤버변수로 등록-> 추후 암호화때 이 경로를 참조함.
				// 파일경로 + 파일명 조합
				TCHAR programpath[_MAX_PATH];
				GetCurrentDirectory(_MAX_PATH, programpath);
				hThis->m_valOriginFolderPath.Format(_T("%s\\"), programpath);
				hThis->m_valOriginFolderPath += hThis->m_origFileName;
			}
			else
			{
				//std::cout << CPrePackagerDlg::currentDateTime() << "s3 download error: " <<
				//	get_object_outcome.GetError().GetExceptionName() << " " <<
				//	get_object_outcome.GetError().GetMessage() << std::endl;
				hThis->Logger(CPrePackagerDlg::currentDateTime() + "s3 download error: " +
					get_object_outcome.GetError().GetExceptionName() + " " +
					get_object_outcome.GetError().GetMessage() + "\n");
				hThis->runSignal = CPrePackagerDlg::RunSignal::STAT_RUN_STOP;
			}
			

		}
		Aws::ShutdownAPI(options);

	}

	// 4. encrypt downloaded file.
	if (hThis->m_valEncUnitSelRadio == 0) // encrypt for just 1 file, not folder mode 
	{
		// 암호화될 파일이 저장될 경로 지정
		TCHAR programpath[_MAX_PATH];
		GetCurrentDirectory(_MAX_PATH, programpath);
		hThis->m_valEncFolderPath = programpath;

		// 암호화 과정 로그창 세팅
		hThis->m_ctrLogListBox.ResetContent();
		hThis->m_ctrLogListBox.AddString(_T("Enc Start Command"));
		hThis->m_bEncryptCancel = FALSE;

		//암호화 쓰레드 시작
		hThis->GetDlgItem(IDC_ENCSTART_BUTTON)->EnableWindow(FALSE);
		hThis->GetDlgItem(IDC_ENCEND_BUTTON)->EnableWindow(TRUE);

		hThis->EncryptProcess();

	}

	// 5. multipart file upload -- 프로그레스 바가 100%가 되면 MultiUpload()가 실행됨.
	// 6. notify to nemodax server with curl -- notifyToServer()


	//hThis->UpdateData(FALSE); // 왜인지 모르겠으나 이것이 있으면 스레드 반복이 안되고 exception이 남.. 
	hThis->runSignal = CPrePackagerDlg::RunSignal::STAT_STOP_RUN;
	return 0;
}


void CPrePackagerDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nIDEvent)
	{
		case BATCHJOB_TIMER:
			SetTimer(BATCHJOB_TIMER, 1000, 0); // 1000 = 1초
			switch (runSignal) {
				case STAT_STOP_RUN:
				case STAT_RUN_WAIT:
				{
					runSignal = STAT_RUN_RUN;
					//cout <<  currentDateTime() << "<<<<new workflow start>>>> \n";
					CPrePackagerDlg::Logger(currentDateTime() + "<<<<new workflow start>>>> \n");
					disableAllButtons();

					unsigned batchJobThreadID = 0;

					m_hBatchJobThread = (HANDLE)_beginthreadex(NULL, 0, &periodicThreadFunc, this, 0, &batchJobThreadID);
					if (m_hBatchJobThread == NULL) {
						//cout <<  currentDateTime() << "BatchJob Thread create error\n";
						CPrePackagerDlg::Logger(currentDateTime() + "BatchJob Thread create error\n");
					}
					
					CloseHandle(m_hBatchJobThread);
					break;
				}

				case STAT_STOP_STOP:
				case STAT_RUN_STOP:
				{
					// terminate batchjob work thread logic
					runSignal = STAT_STOP_STOP;
					KillTimer(BATCHJOB_TIMER);
					::TerminateThread(m_hBatchJobThread, 0);
					enableAllButtons();
					GetDlgItem(IDC_FETCHLIST_BUTTON)->SetWindowTextW(_T("Encryption Batch Job Start"));
					break;
				}
				case STAT_RUN_RUN:
					//just pass
					break;
			}
		break;
	}


	flush(m_logOut);
}

void CPrePackagerDlg::OnBnClickedCognitoRun()
{
	//아직 테스트중... 런타임 에러가 남..
	
	/*
	Aws::String value = "{\"aws_access_key_id\": \"your_access_key_id\",\"aws_secret_access_key\" : \"your_secret_access_key\"}";
	//Aws::String value = "ap-northeast-2:d8a2e22a-ac34-46d4-b1d7-4582f37877bb";
	Aws::Utils::Json::JsonValue jsonValue = Aws::Utils::Json::JsonValue(value);
	Aws::Utils::Json::JsonView jsonView = Aws::Utils::Json::JsonView(jsonValue);
	Aws::CognitoIdentity::Model::Credentials credentials = Aws::CognitoIdentity::Model::Credentials(jsonView);
	*/

	Aws::Client::ClientConfiguration clientConfig;
	clientConfig.region = "ap-northeast-2";
	
	auto cognitoCachingAnonymousCredentialsProvider = Aws::MakeShared<Aws::Auth::CognitoCachingAnonymousCredentialsProvider>(
		COGNITO_ALLOCATION_TAG, "identityPoolId", "ap-northeast-2:d8a2e22a-ac34-46d4-b1d7-4582f37877bb");

	auto s3client = Aws::MakeShared<S3Client>( COGNITO_ALLOCATION_TAG, cognitoCachingAnonymousCredentialsProvider, clientConfig);
}


void CPrePackagerDlg::OnBnClickedFetchlistButton()
{


	
	switch (runSignal) {
		//강제 종료
	case STAT_RUN_RUN:
	case STAT_RUN_STOP:
	case STAT_RUN_WAIT:
		runSignal = STAT_STOP_STOP;
		KillTimer(BATCHJOB_TIMER);
		::TerminateThread(this->m_hBatchJobThread, 0);
		enableAllButtons();
		GetDlgItem(IDC_FETCHLIST_BUTTON)->SetWindowTextW(_T("Encryption Batch Job Start"));
		CPrePackagerDlg::Logger(currentDateTime() + "terminated\n");
		//cout << currentDateTime() << "terminated\n";
		//logOut << currentDateTime << "\n";
		break;

		//강제 시작
	case STAT_STOP_RUN:
	case STAT_STOP_STOP:
		runSignal = STAT_STOP_RUN;
		disableAllButtons();
		GetDlgItem(IDC_FETCHLIST_BUTTON)->SetWindowTextW(_T("Stop"));
		SetTimer(BATCHJOB_TIMER, 200, 0); 
		CPrePackagerDlg::Logger(currentDateTime() + "start new batch job\n");
		
		//cout <<  currentDateTime() <<  "start new batch job\n";
		break;
	}
	flush(m_logOut);
}

void CPrePackagerDlg::Logger(std::string str) {
	cout << str;
	m_logOut << str ;
}


void CPrePackagerDlg::MultiUpload()
{
	//5. multipart file upload -- 프로그레스 바가 100 % 가 되면 MultiUpload()가 실행됨.
	
	Aws::SDKOptions options;
	options.loggingOptions.logLevel = Aws::Utils::Logging::LogLevel::Trace;
	
	Aws::InitAPI(options);

	Aws::String bucket = "nemodax-dev";

	// Set up S3 client
	ClientConfiguration config;
	config.region = "ap-northeast-2";
	config.connectTimeoutMs = 3000;
	config.requestTimeoutMs = 60000;
	
	
	auto s3Client = Aws::MakeShared<S3Client>("MULTIPART_UPLOAD", config);

	auto sdk_client_executor = Aws::MakeShared<Aws::Utils::Threading::DefaultExecutor>("MULTIPART_UPLOAD");
	Aws::Transfer::TransferManagerConfiguration transferConfig(sdk_client_executor.get());
	
	transferConfig.s3Client = s3Client;
	
	std::mutex progress_mtx_lock;
	bool progressWritten = false;
	transferConfig.uploadProgressCallback =
		[&](const Aws::Transfer::TransferManager*, const std::shared_ptr<const Aws::Transfer::TransferHandle>&transferHandle)
	{
		progress_mtx_lock.lock();
		if (transferHandle->GetBytesTransferred() == transferHandle->GetBytesTotalSize() && progressWritten == false)	{
			progressWritten = true;
			std::ostringstream osstr;
			osstr << currentDateTime() << "Upload Progress: " << transferHandle->GetBytesTransferred() << " of " << transferHandle->GetBytesTotalSize() << " bytes\n";
			CPrePackagerDlg::Logger(osstr.str());
		}
		else 
			std::cout << currentDateTime() << "Upload Progress: " << transferHandle->GetBytesTransferred() << " of " << transferHandle->GetBytesTotalSize() << " bytes\n";
		
		COORD curPosition = getXY();
		gotoXY(0, curPosition.Y - 1);
		progress_mtx_lock.unlock();
	};

	//static std::shared_ptr<Aws::Transfer::TransferManager> transferManager = Aws::Transfer::TransferManager::Create(transferConfig);// 반복작업으로 돌렸더니 런타임 예외가 나서 static 없앴더니 되네...? 반복될때마다 같은 이름의 static 변수를 새로 생성해서 그랬나?
	std::shared_ptr<Aws::Transfer::TransferManager> transferManager = Aws::Transfer::TransferManager::Create(transferConfig);
	
	
	//Aws::String fileToUpload = std::string(CT2CA(m_origFileName.operator LPCWSTR())); //CString -> std::string -> Aws::String
	Aws::String fileToUpload = (Aws::String) m_strTargetPath;
	fileToUpload.append(".MS4"); 
	m_origFileNameString.append(".MS4");
	std::shared_ptr<Aws::Transfer::TransferHandle> transferHandle = transferManager->UploadFile((Aws::String)m_origFileNameString, bucket.c_str(), fileToUpload.c_str(),
		"multipart/form-data", Aws::Map<Aws::String, Aws::String>());
	
	size_t retries = 0;
	transferHandle->WaitUntilFinished();
	while (transferHandle->GetStatus() == Aws::Transfer::TransferStatus::FAILED && retries++ < 5)
	{
		transferManager->RetryUpload(fileToUpload.c_str(), transferHandle);
		transferHandle->WaitUntilFinished();
	}
	transferHandle->SetIsMultipart(true);

	Aws::Transfer::PartStateMap completedParts = transferHandle->GetCompletedParts();

	COORD curPosition = getXY();
	gotoXY(0, curPosition.Y + 1);
	for (const auto &p : completedParts)
	{
		//std::cout <<  currentDateTime() << fileToUpload << "\n" << "completedparts[" << p.first << "]" << p.second->GetETag() << '\n';
		std::ostringstream osstr;
		osstr << currentDateTime() << fileToUpload << "\n" << "completedparts[" << p.first << "]" << p.second->GetETag() << '\n';
		CPrePackagerDlg::Logger(osstr.str());
	}

	Aws::Transfer::TransferStatus status = transferHandle->GetStatus();
	std::string res("Result Status:");
	res.append(std::to_string((int)status));

	//std::cout << currentDateTime() << res.c_str() << "\n";
	//std::cout << currentDateTime() << "upload is done" << "\n";
	CPrePackagerDlg::Logger(currentDateTime() + res.c_str() + "\n");
	CPrePackagerDlg::Logger(currentDateTime() + "upload is done" + "\n");
	
	
	//업로드 객체에 대한 퍼블릭 권한 설정
	{
		ClientConfiguration config;
		config.region = "ap-northeast-2";
		Aws::S3::S3Client s3_client(config);
		
		Aws::S3::Model::GetObjectAclRequest get_request;
		get_request.SetBucket(bucket);
		get_request.SetKey(fileToUpload);
		auto get_outcome = s3_client.GetObjectAcl(get_request);

		if (get_outcome.IsSuccess())
		{
			//Aws::S3::Model::Grantee grantee;
			//grantee.SetEmailAddress("hapsody@gmail.com");
			Aws::S3::Model::PutObjectAclRequest put_request;
			
			put_request.SetACL(Aws::S3::Model::ObjectCannedACL::public_read);
			put_request.SetBucket(bucket);
			put_request.SetKey(fileToUpload);
			s3_client.PutObjectAcl(put_request);
		}
		else
		{
			std::cout << "GetObjectAcl error: "
				<< get_outcome.GetError().GetExceptionName() << " - "
				<< get_outcome.GetError().GetMessage() << std::endl;
		}
	}
	


	Aws::ShutdownAPI(options);
	
	CFile::Remove((LPCTSTR)m_origFileName);
	m_origFileName.Append(_T(".MS4"));
	CFile::Remove((LPCTSTR)m_origFileName);
	
	notifyToServer();
}


void CPrePackagerDlg::notifyToServer()
{
	// 6. notify to nemodax server with curl -- notifyToServer()
	//std::cout <<  currentDateTime() << "Notify To Server! : ";
	CPrePackagerDlg::Logger(currentDateTime() + "Notify To Server! : ");
	std::string strTargetURL;
	std::string strResourceJSON;

	struct MemoryStruct chunk;

	chunk.memory = (char *)malloc(1); /* will be grown as needed by the realloc above */
	chunk.size = 0; /* no data at this point */
	
	strTargetURL = "http://dev.nemodax.com/api/file/updateEncryptComplete.do?key=qpalmfheuzjewgff&sale_file_no=" + std::to_string(iSaleFileNo) + "&encrypt_type=TERUTEN";
	//std::cout <<  currentDateTime() << strTargetURL << "\n";
	CPrePackagerDlg::Logger(currentDateTime() + strTargetURL + "\n");

	curl_global_init(CURL_GLOBAL_ALL);

	m_curl = curl_easy_init();
	if (m_curl)
	{
		char errbuf[CURL_ERROR_SIZE];

		curl_easy_setopt(m_curl, CURLOPT_URL, strTargetURL.c_str());
		curl_easy_setopt(m_curl, CURLOPT_ERRORBUFFER, errbuf);
		curl_easy_setopt(m_curl, CURLOPT_FRESH_CONNECT, 1L); // 효과 없었음
		curl_easy_setopt(m_curl, CURLOPT_FORBID_REUSE, 1L); // 효과 없었음

		// 결과 기록
		curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, &CPrePackagerDlg::WriteMemoryCallback);
		curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, (void *)&chunk);

		m_curlRes = curl_easy_perform(m_curl);
		
		if (m_curlRes != CURLE_OK)
		{
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(m_curlRes));
			//std::cout <<  currentDateTime() << errbuf << std::endl;
			CPrePackagerDlg::Logger(currentDateTime() + errbuf + "\n");

		}
		//std::cout << "------------Result" << std::endl;
		//std::cout << chunk.memory << std::endl;
		std::string strChunk(chunk.memory);
		CPrePackagerDlg::Logger(currentDateTime() + "------------Result\n");
		CPrePackagerDlg::Logger(strChunk + "\n");

		curl_easy_reset(m_curl);

	}
	
	//std::cout << "\n\n";
	CPrePackagerDlg::Logger("\n\n");
	
}

size_t CPrePackagerDlg::WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;

	char *ptr = (char *)realloc(mem->memory, mem->size + realsize + 1);
	if (ptr == NULL) {
		/* out of memory! */
		printf("not enough memory (realloc returned NULL)\n");
		return 0;
	}

	mem->memory = ptr;
	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}


void CPrePackagerDlg::disableAllButtons() {
	GetDlgItem(IDC_ENCSTART_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_ENCEND_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_ORIGINSEL_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_ENCSEL_BUTTON)->EnableWindow(FALSE);
	//GetDlgItem(IDC_FILESEL_RADIO)->EnableWindow(FALSE);
	//GetDlgItem(IDC_FOLDERSEL_RADIO)->EnableWindow(FALSE);
	//GetDlgItem(IDC_KEYTYPE_MAC)->EnableWindow(FALSE);
	//GetDlgItem(IDC_KEYTYPE_STREAMING)->EnableWindow(FALSE);
	GetDlgItem(IDC_DOWNLOAD_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_UPLOAD_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_MULTIUPLOAD_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_COGNITO_RUN)->EnableWindow(FALSE);
//	GetDlgItem(IDC_FETCHLIST_BUTTON)->EnableWindow(FALSE);
	GetDlgItem(IDC_MULTIDOWNLOAD_BUTTON)->EnableWindow(FALSE);
}

void CPrePackagerDlg::enableAllButtons() {
	GetDlgItem(IDC_ENCSTART_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_ENCEND_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_ORIGINSEL_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_ENCSEL_BUTTON)->EnableWindow(TRUE);
	//GetDlgItem(IDC_FILESEL_RADIO)->EnableWindow(TRUE);
	//GetDlgItem(IDC_FOLDERSEL_RADIO)->EnableWindow(TRUE);
	//GetDlgItem(IDC_KEYTYPE_MAC)->EnableWindow(TRUE);
	//GetDlgItem(IDC_KEYTYPE_STREAMING)->EnableWindow(TRUE);
	GetDlgItem(IDC_DOWNLOAD_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_UPLOAD_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_MULTIUPLOAD_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_COGNITO_RUN)->EnableWindow(TRUE);
	//GetDlgItem(IDC_FETCHLIST_BUTTON)->EnableWindow(TRUE);
	GetDlgItem(IDC_MULTIDOWNLOAD_BUTTON)->EnableWindow(TRUE);
}




// 현재시간을 string type으로 return하는 함수
const std::string CPrePackagerDlg::currentDateTime() {
	time_t     now = time(0); //현재 시간을 time_t 타입으로 저장
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "[%Y-%m-%d.%X] ", &tstruct); // YYYY-MM-DD.HH:mm:ss 형태의 스트링

	return buf;
}

