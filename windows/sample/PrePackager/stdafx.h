
// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.

#pragma once

#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // 거의 사용되지 않는 내용은 Windows 헤더에서 제외합니다.
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // 일부 CString 생성자는 명시적으로 선언됩니다.

// MFC의 공통 부분과 무시 가능한 경고 메시지에 대한 숨기기를 해제합니다.
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC 핵심 및 표준 구성 요소입니다.
#include <afxext.h>         // MFC 확장입니다.


#include <afxdisp.h>        // MFC 자동화 클래스입니다.



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // Internet Explorer 4 공용 컨트롤에 대한 MFC 지원입니다.
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // Windows 공용 컨트롤에 대한 MFC 지원입니다.
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC의 리본 및 컨트롤 막대 지원



#pragma comment(lib, "wininet.lib")

#ifdef _DEBUG
#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console")
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console")
#endif
#endif
/* 
//vcpkg
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\vcpkg\\packages\\aws-sdk-cpp_x86-windows\\lib\\aws-cpp-sdk-core.lib")
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\vcpkg\\packages\\aws-sdk-cpp_x86-windows\\lib\\aws-cpp-sdk-dynamodb.lib")
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\vcpkg\\packages\\aws-sdk-cpp_x86-windows\\lib\\aws-cpp-sdk-kinesis.lib")
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\vcpkg\\packages\\aws-sdk-cpp_x86-windows\\lib\\aws-cpp-sdk-s3.lib")
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\vcpkg\\packages\\aws-sdk-cpp_x86-windows\\lib\\aws-cpp-sdk-transfer.lib")
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\vcpkg\\packages\\aws-sdk-cpp_x86-windows\\lib\\aws-cpp-sdk-cognito-identity.lib")
*/

//nuget
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\packages\\AWSSDKCPP-Core.1.6.25\\build\\native\\lib\\Win32\\Debug\\v141\\dynamic\\aws-cpp-sdk-core.lib")
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\packages\\AWSSDKCPP-CognitoIdentity.1.6.20140630.25\\build\\native\\lib\\Win32\\Debug\\v141\\dynamic\\aws-cpp-sdk-cognito-identity.lib") 
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\packages\\AWSSDKCPP-S3.1.6.20060301.25\\build\\native\\lib\\Win32\\Debug\\v141\\dynamic\\aws-cpp-sdk-s3.lib") 
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\packages\\AWSSDKCPP-Transfer.1.6.25\\build\\native\\lib\\Win32\\Debug\\v141\\dynamic\\aws-cpp-sdk-transfer.lib") 
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\packages\\AWSSDKCPP-IdentityManagement.1.6.25\\build\\native\\lib\\Win32\\Debug\\v141\\dynamic\\aws-cpp-sdk-identity-management.lib") 
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\packages\\AWSSDKCPP-DynamoDB.1.6.20120810.25\\build\\native\\lib\\Win32\\Debug\\v141\\dynamic\\aws-cpp-sdk-dynamodb.lib") 

#pragma comment(lib, "wldap32.lib")
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib, "C:\\Users\\june\\source\\repos\\cppencryptbatchservice\\windows\\sample\\PrePackager\\packages\\libcurl\\libcurl.lib")




#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


