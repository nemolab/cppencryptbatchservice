package com.teruten.download;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.teruten.common.DRMINFO;
import com.teruten.common.Define;
import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tplayertestapp_tea_as.BuildConfig;
import com.teruten.tplayertestapp_tea_as.R;

import java.io.File;

public class LocalRecryptActivity extends AppCompatActivity implements View.OnClickListener{

    //태그
    final public static String TAG = "TplayerTest";
    final public static String strDefaultDownloadPath
            = Environment.getExternalStorageDirectory().getAbsolutePath() + "/teruten/";

    private EditText edtOriFileName, edtEncFileName, edtMac;

    // 임시 경로
    File downloadDir;
    // DRM INFO
    DRMINFO _info = DRMINFO.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_recrypt);

        if (!checkPermissions() && Build.VERSION.SDK_INT >= 23) {
            requestPermissions();
        } else {
            // 초기화
            initLayout();
            setDeviceData();
        }

        // 다운로드 폴더가 없으면 생성
        downloadDir = new File(strDefaultDownloadPath);
        if (!downloadDir.exists())
            downloadDir.mkdirs();
    }

    @Override
    public void onClick(View v) {
        int nStandardTime = (int)(System.currentTimeMillis() / 1000);
        int nRtn = -1;

        switch (v.getId()) {
            case R.id.btnMakePrepackagingFile:
                // 프리패키징 파일 만들기
                nRtn = TMS4Encrypt.TMS4EFileEncrypt(
                        downloadDir.getPath() + "/" + edtOriFileName.getText().toString(),   // 파일경로
                        TMS4Encrypt.TMS4_HINT_PREPACK,                                          // 정책 값
                        "",                                                                 // 기기 값
                        "",                                                                 // 맥어드레스 값
                        "android user",                                                     // 사용자 ID
                        "android url",                                                      // 컨텐츠 URL
                        "android watermark",                                                // 워터마크 정보
                        nStandardTime,                                                          // 현재시간
                        150,                                                                // 기간
                        150,                                                                // 횟수
                        7);                                                                 // 서비스 레벨

                displayMessage("TMS4EFileEncrypt Return : " + nRtn);
                break;

            case R.id.btnMakeMS4File:
                // Mac 값으로 MS4 만들기
                nRtn = TMS4Encrypt.TMS4EFileEncrypt(
                        downloadDir.getPath() + "/" + edtOriFileName.getText().toString(),   // 파일경로
                        TMS4Encrypt.TMS4_POLICY_MAC,                                            // 정책 값
                        "",                                                                 // 기기 값
                        edtMac.getText().toString(),                                            // 맥어드레스 값
                        "android user",                                                     // 사용자 ID
                        "android url",                                                      // 컨텐츠 URL
                        "android watermark",                                                // 워터마크 정보
                        nStandardTime,                                                          // 현재시간
                        300,                                                                // 기간
                        100,                                                                // 횟수
                        7);                                                                 // 서비스 레벨

                displayMessage("TMS4EFileEncrypt Return : " + nRtn);
                break;

            case R.id.btnRecrypt:
                nRtn = TMS4Encrypt.TMS4ERecryptEx(
                        downloadDir.getPath() + "/" + edtEncFileName.getText().toString(),   // 파일경로
                        TMS4Encrypt.TMS4_POLICY_MAC,                                            // 정책 값
                        "",                                                                 // 기기 값
                        edtMac.getText().toString(),                                            // 맥어드레스 값
                        "UserA",                                                            // 사용자 ID
                        "UrlA",										// 컨텐츠 URL
                        "WatermarkA",										// 워터마크정보
                        "UuidA",										// uuid
                        "sellerA",								// sellerId
                        "cidA",									// sellerContentId
                        2,										// companyCode
                        nStandardTime,                                                          // 현재시간
                        30,                                                                 // 기간
                        0,                                                                  // 횟수
                        0);                                                                 // 서비스 레벨

                displayMessage("Recrypt Return : " + nRtn);
                break;

            case R.id.btnPolocyRecrypt:
                nRtn = TMS4Encrypt.TMS4EPolicyRecrypt(
                        downloadDir.getPath() + "/" + edtEncFileName.getText().toString(),
                        "",
                        edtMac.getText().toString(),
                        0,                                      // 지금부터 몇일이나 재생 할건지.
                        false,                                  //true 마지막 시간에 추가   false 새 기간 설정
                        10,                                     // 지금부터 몇회나 재생 할건지.
                        false,                                  //true 기존 횟수에 추가  false 새 횟수 설정
                        TMS4Encrypt.TMS4_POLICY_TYPE_COUNT,         // 기간 연장할 경우 (TMS4_POLICY_TYPE_PERIOD)   횟수 연장할 경우 (TMS4_POLICY_TYPE_COUNT)
                        nStandardTime);

                displayMessage("Policy Recrypt Return : " + nRtn);
                break;
        }

        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(downloadDir));
        sendBroadcast(intent);
    }

    private void initLayout() {
        Button btnMakePrepackagingFile = (Button) findViewById(R.id.btnMakePrepackagingFile);
        Button btnMakeMS4File = (Button) findViewById(R.id.btnMakeMS4File);
        Button btnRecrypt = (Button) findViewById(R.id.btnRecrypt);
        Button btnPolocyRecrypt = (Button) findViewById(R.id.btnPolocyRecrypt);
        edtOriFileName = (EditText)findViewById(R.id.edtOriFileName);
        edtEncFileName = (EditText)findViewById(R.id.edtEncFileName);
        edtMac = (EditText) findViewById(R.id.edtMac);

        btnMakePrepackagingFile.setOnClickListener(this);
        btnMakeMS4File.setOnClickListener(this);
        btnRecrypt.setOnClickListener(this);
        btnPolocyRecrypt.setOnClickListener(this);
    }

    // Toast 로 메시지 출력
    private void displayMessage(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("HardwareIds")
    private void setDeviceData() {
        TelephonyManager tpman = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiinfo = null;
        if (wifiManager != null) {
            wifiinfo = wifiManager.getConnectionInfo();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if(tpman == null && wifiinfo == null) return;

        _info.HINT = TMS4Encrypt.TMS4_HINT_PREPACK;

        if( tpman != null) {
            if (tpman.getDeviceId() != null && tpman.getDeviceId().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_IMEI;
                _info.IMEI = tpman.getDeviceId();
                Log.d(TAG, "Index___Imei : " + _info.IMEI + ", " + _info.IMEI.length());
            }
        }

        if(wifiinfo != null) {
            if (wifiinfo.getMacAddress() != null && wifiinfo.getMacAddress().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_MAC_ADDRESS;
                _info.MACADDRESS = wifiinfo.getMacAddress();

                Log.d(TAG, "Index___MacAddress : " + _info.MACADDRESS + ", " + _info.MACADDRESS.length());
            }
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) +
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProviceRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if( shouldProviceRationale ) {
            new AlertDialog.Builder(this)
                    .setTitle("INFO")
                    .setMessage("단말기 정보 및 저장소 권한이 필요합니다.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startPermissionRequest();
                        }
                    })
                    .create()
                    .show();
        } else {
            startPermissionRequest();
        }
    }

    private void startPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[] {Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, Define.REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case Define.REQUEST_PERMISSIONS_REQUEST_CODE:
                if( grantResults.length > 0) {
                    // 초기화
                    initLayout();
                    setDeviceData();
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("INFO")
                            .setMessage("단말기 정보 및 저장소 권한이 필요합니다.")
                            .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null);
                                    intent.setData(uri);
                                    intent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            })
                            .create()
                            .show();
                }
        }
    }
}
