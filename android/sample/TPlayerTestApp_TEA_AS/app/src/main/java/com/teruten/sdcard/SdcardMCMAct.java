package com.teruten.sdcard;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.teruten.mcm.TMCM;
import com.teruten.mcm.TMCMActivity;
import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayertestapp_tea_as.R;

import static com.teruten.common.Define.VIEWLOG;

public class SdcardMCMAct extends TMCMActivity
{
	VideoView _video = null;
	private TMS4WebServer _webServer;
	String TAG = "VideoDRMAct";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.video);
		_video = (VideoView)findViewById(R.id.video);
		MediaController mc = new MediaController(this);
		_video.setMediaController(mc);

		String strMediaPath = getIntent().getStringExtra("PATH");

		/***************************** DRM ***********************/
		if (strMediaPath.endsWith(".MS4")) {
			int nReturn;

			// 초기화
			if( DRM_Init() != TMS4Encrypt.TMS4E_OK) return;

			// 기기 인증
			/*nReturn = _webServer.isValidDevice();
			if(nReturn != 0)
			{
				displayMessage("유효하지 않은 기기입니다 : " + nReturn);
				if(VIEWLOG) Log.d(TAG, "Faile Accredit : " + Integer.toString(nReturn));
				return;
			}*/

			// 파일 인증받기 - 원격 파일
			if (strMediaPath.startsWith("http://")) {

			}
			// 파일 인증받기 - 로컬 파일
			else
			{
			nReturn = _webServer.MediaAccreditation(strMediaPath);
			if (nReturn != 0) {
				displayMessage("open local media error : " + nReturn);
				if(VIEWLOG) Log.d(TAG, "Faile Accredit : " + Integer.toString(nReturn));
				return;
			}

			if(VIEWLOG) Log.d("TERUTEN", "--------------------------");
			if(VIEWLOG) Log.d("TERUTEN", _webServer.GetWatermark());
			if(VIEWLOG) Log.d("TERUTEN", "--------------------------");
			if(VIEWLOG) Log.d("TERUTEN", _webServer.GetUrl());
			if(VIEWLOG) Log.d("TERUTEN", "--------------------------");
			if(VIEWLOG) Log.d("TERUTEN", _webServer.GetUserId());
			if(VIEWLOG) Log.d("TERUTEN", "--------------------------");
		}

			// DRM 용 파일 경로 가져오기.
			strMediaPath = _webServer.getFilePath(strMediaPath);
		}

		_video.setVideoPath(strMediaPath);
		_video.start();
		if(VIEWLOG) Log.e("VideoAct", "media path : " + strMediaPath);

		_video.setOnCompletionListener(new OnCompletionListener() {

			public void onCompletion(MediaPlayer mp) {
				Log.e("VideoAct", "completion.");
			}
		});
		_video.setOnErrorListener(new OnErrorListener() {

			public boolean onError(MediaPlayer mp, int what, int extra) {
				if(VIEWLOG) Log.e("VideoAct", "error.");
				return false;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		if(VIEWLOG) Log.e(TAG, "video act onDestroy");
		if(_webServer != null)
		{
			try {
				_webServer.StopServer();
			} catch (Exception e) {
				e.printStackTrace();
			}
			_webServer = null;
		}

		super.onDestroy();
	}


	/**********************************************************
	 * DRM 관련 함수
	 **********************************************************/
	private int DRM_Init() {
		int nReturn = -1;

		if (_webServer == null) {
			if(VIEWLOG) Log.e(TAG, "web server start");
			_webServer = new TMS4WebServer();
			//nReturn = _webServer.InitInstance(this, "SiwonA01");
            nReturn = _webServer.InitInstance(this, "teruten");
			if(nReturn != TMS4Encrypt.TMS4E_OK)
			{
				displayMessage("DRM init err : " + nReturn);
				if(VIEWLOG) Log.e(TAG, "DRM init err : " + nReturn);
				return nReturn;
			}
			_webServer.StartServer(50000);
		}
		return nReturn;
	}

	/**********************************************************
	 * 기타 함수
	 **********************************************************/
	// Toast 로 메시지 출력
	public void displayMessage(String message){
		if(message != null){
			Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
		}
	}


	/**********************************************************
	 * MCM 관련
	 **********************************************************/

	@Override
	protected void messageProcess(int msg, String packageName) {
		if(VIEWLOG) Log.e("TMCMSample", "messageProcess msg[" + msg + "]");
		String message = null;
		switch (msg) {
			case TMCM.MESSAGE_INIT:
				message = getString(R.string.msg_init);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_START:
				message = getString(R.string.msg_start);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_TIMEOUT:
				message = getString(R.string.msg_timeout);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_ROOTING:
				message = getString(R.string.msg_rooting);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_EMULATOR:
				message = getString(R.string.msg_emulator);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_HDMI:
				message = getString(R.string.msg_hdmi);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_MIRACAST:
				message = getString(R.string.msg_miracast);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_USB:
				message = getString(R.string.msg_usb);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_BLUETOOTH:
				message = getString(R.string.msg_bluetooth);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_DATANET:
				message = getString(R.string.msg_data);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_WIFI:
				message = getString(R.string.msg_wifi);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_PROCESSCHECK:
				message = getString(R.string.msg_process);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_REMOTECHECK:
				message = getString(R.string.msg_remote);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_GPS:
				message = getString(R.string.msg_gps);
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				break;
			case TMCM.MESSAGE_NONE:
			case TMCM.MESSAGE_READY:
				return;
			case TMCM.MESSAGE_SHORTKEY:
				Toast.makeText(this, "캡쳐가 되었습니다.", Toast.LENGTH_LONG).show();
				break;
			case TMCM.MESSAGE_RECORD:
				message = getString(R.string.msg_recode)+"\n"+packageName;
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				//finish();
				break;
			case TMCM.MESSAGE_AUDIO_RECORD	:
				message = getString(R.string.msg_audio_recode)+"\n"+packageName;
				Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
				//finish();
				break;
			default:
				return;
		}
	}

}