package com.teruten.download;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.PlaybackParams;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.teruten.common.DRMINFO;
import com.teruten.common.Define;
import com.teruten.mcm.TMCM;
import com.teruten.mcm.TMCMActivity;
import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayertestapp_tea_as.R;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static com.teruten.common.Define.DEVELOPER_MODE;
import static com.teruten.common.Define.VIEWLOG;

public class ExoPlayerAct extends TMCMActivity implements View.OnClickListener{

    private static final String TAG = "ExoPlayerAct";
    private TMS4WebServer _webServer;
    private SimpleExoPlayer player;
    private TextView speed025, speed05, speed075, speedNormal, speed125, speed15, speed2, speed3;
    private ConstraintLayout speedRateItemView;
    private int defaultSpeedRateBackgroundColor;
    private DRMINFO _info = DRMINFO.getInstance();
    private int drmType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exo_player);

        initLayout();

        String strMediaPath = getIntent().getStringExtra("PATH");
        drmType = getIntent().getIntExtra("drmType", 0);

        PlayerView simpleExoPlayerView = findViewById(R.id.exoplayer);

        // 1. Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();

        // 2. Create the player
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);

        // Bind the player to the view.
        simpleExoPlayerView.setUseController(true);
        simpleExoPlayerView.requestFocus();
        simpleExoPlayerView.setPlayer(player);

        /***************************** DRM ***********************/
        if (strMediaPath.endsWith(".MS4")) {
            int nReturn = -1;

            // 초기화
            if (DRM_Init() != TMS4Encrypt.TMS4E_OK) return;

            // 파일 인증받기 - 원격 파일
            if (strMediaPath.startsWith("http://") || strMediaPath.startsWith("https://")) {
                nReturn = _webServer.OpenRemoteMedia(strMediaPath, "nemolabs01");
                if (nReturn != TMS4Encrypt.TMS4E_OK) {
                    displayMessage("open remote media error : " + nReturn);
                    return;
                }
            }
            // 파일 인증받기 - 로컬 파일
            else {

                switch (drmType) {
                    case Define.DOWNLOAD:
                        int nTime = (int) (System.currentTimeMillis() / 1000) + 1;
                        nReturn = _webServer.MediaAccreditation(strMediaPath, nTime);
                        break;
                    case Define.SDCARD:
                        nReturn = _webServer.MediaAccreditation(strMediaPath);
                        break;
                }

                if (nReturn != 0) {
                    displayMessage("open local media error : " + nReturn);
                    if(VIEWLOG) Log.d(TAG, "Faile Accredit : " + Integer.toString(nReturn));
                    return;
                }

                DRM_showInfo();
            }

            // DRM 용 파일 경로 가져오기.
            strMediaPath = _webServer.getFilePath(strMediaPath);
        }

        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "TPlayerTestApp_TEA_AS"), null);


        // URI
        Uri mp4VideoUri = Uri.parse(strMediaPath);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mp4VideoUri);

        // Prepare the player with the source.
        if(VIEWLOG) Log.d("TEURTEN", "EXOPLAYER RUN!!!");
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        setVideoSpeed(1.0f, R.id.speed4);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.exo_settings:
                if(speedRateItemView.getVisibility() == View.VISIBLE)
                    speedRateItemView.setVisibility(View.INVISIBLE);
                else
                    speedRateItemView.setVisibility(View.VISIBLE);
                break;
            case R.id.exo_fullscreen:
                Toast.makeText(this, "FullScreen On!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.speed1:
                clearAll();
                setVideoSpeed(0.25f, R.id.speed1);
                break;
            case R.id.speed2:
                clearAll();
                setVideoSpeed(0.5f, R.id.speed2);
                break;
            case R.id.speed3:
                clearAll();
                setVideoSpeed(0.75f, R.id.speed3);
                break;
            case R.id.speed4:
                clearAll();
                setVideoSpeed(1.0f, R.id.speed4);
                break;
            case R.id.speed5:
                clearAll();
                setVideoSpeed(1.25f, R.id.speed5);
                break;
            case R.id.speed6:
                clearAll();
                setVideoSpeed(1.5f, R.id.speed6);
                break;
            case R.id.speed7:
                clearAll();
                setVideoSpeed(2.0f, R.id.speed7);
                break;
            case R.id.speed8:
                clearAll();
                setVideoSpeed(3.0f, R.id.speed8);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if(VIEWLOG) Log.e(TAG, "video act onDestroy");
        if(player != null) {
            player.release();
        }

        if (_webServer != null) {
            try {
                _webServer.StopServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
            _webServer = null;
        }

        super.onDestroy();
    }

    private void setVideoSpeed(float value, int textViewId) {
        TextView textview = (TextView)findViewById(textViewId);
        textview.setBackgroundColor(Color.parseColor("#FF8000"));

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            PlaybackParams params = new PlaybackParams();
            params.setSpeed(value);
            player.setPlaybackParams(params);
        }
    }

    private void clearAll() {
        speed025.setBackgroundColor(defaultSpeedRateBackgroundColor);
        speed05.setBackgroundColor(defaultSpeedRateBackgroundColor);
        speed075.setBackgroundColor(defaultSpeedRateBackgroundColor);
        speedNormal.setBackgroundColor(defaultSpeedRateBackgroundColor);
        speed125.setBackgroundColor(defaultSpeedRateBackgroundColor);
        speed15.setBackgroundColor(defaultSpeedRateBackgroundColor);
        speed2.setBackgroundColor(defaultSpeedRateBackgroundColor);
        speed3.setBackgroundColor(defaultSpeedRateBackgroundColor);
    }

    /**********************************************************
     * UI Initial
     **********************************************************/
    private void initLayout() {
        speed025 = (TextView) findViewById(R.id.speed1);
        speed05 = (TextView) findViewById(R.id.speed2);
        speed075 = (TextView) findViewById(R.id.speed3);
        speedNormal = (TextView) findViewById(R.id.speed4);
        speed125 = (TextView) findViewById(R.id.speed5);
        speed15 = (TextView) findViewById(R.id.speed6);
        speed2 = (TextView) findViewById(R.id.speed7);
        speed3 = (TextView) findViewById(R.id.speed8);
        ImageButton btnSettings = (ImageButton) findViewById(R.id.exo_settings);
        ImageButton btnFullscreen = (ImageButton) findViewById(R.id.exo_fullscreen);
        speedRateItemView = (ConstraintLayout) findViewById(R.id.speed_rate_item);

        speed025.setOnClickListener(this);
        speed05.setOnClickListener(this);
        speed075.setOnClickListener(this);
        speedNormal.setOnClickListener(this);
        speed125.setOnClickListener(this);
        speed15.setOnClickListener(this);
        speed2.setOnClickListener(this);
        speed3.setOnClickListener(this);
        btnSettings.setOnClickListener(this);
        btnFullscreen.setOnClickListener(this);

        defaultSpeedRateBackgroundColor = speed025.getDrawingCacheBackgroundColor();
    }

    /**********************************************************
     * DRM 관련 함수
     **********************************************************/
    private int DRM_Init() {
        int nReturn = -1;

        if (_webServer == null) {
            if(VIEWLOG) Log.e(TAG, "web server start");
            _webServer = new TMS4WebServer();

            setDeviceData();
            DRMINFO info = DRMINFO.getInstance();
            if(DEVELOPER_MODE) info.MACADDRESS = "90:2b:34:a2:3a:04"; // developer test

            switch (drmType) {
                case Define.DOWNLOAD:
                    nReturn = _webServer.InitInstance(info.IMEI, info.MACADDRESS, this);
                    break;
                case Define.SDCARD:
                    nReturn = _webServer.InitInstance(this, "teruten");
                    break;
                case Define.STREAMING:
                    nReturn = _webServer.InitInstance(this, "siwona01");
                    break;
                default:
                    nReturn = _webServer.InitInstance(info.IMEI, info.MACADDRESS, this);
                    break;
            }

            if (nReturn != TMS4Encrypt.TMS4E_OK) {
                return nReturn;
            }

            _webServer.setBufferSize(1024);
            _webServer.StartServer(50000);
        }
        return nReturn;
    }

    /**********************************************************
     * 기타 함수
     **********************************************************/
    // Toast 로 메시지 출력
    private void displayMessage(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("HardwareIds")
    private void setDeviceData() {
        TelephonyManager tpman = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiinfo = null;
        if (wifiManager != null) {
            wifiinfo = wifiManager.getConnectionInfo();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if(tpman == null && wifiinfo == null) return;

        _info.HINT = TMS4Encrypt.TMS4_HINT_PREPACK;

        if( tpman != null) {
            if (tpman.getDeviceId() != null && tpman.getDeviceId().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_IMEI;
                _info.IMEI = tpman.getDeviceId();
                if(VIEWLOG) Log.d(TAG, "Index___Imei : " + _info.IMEI + ", " + _info.IMEI.length());
            }
        }

        if(wifiinfo != null) {
            if (wifiinfo.getMacAddress() != null && wifiinfo.getMacAddress().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_MAC_ADDRESS;
                _info.MACADDRESS = wifiinfo.getMacAddress();
                if(VIEWLOG) Log.d(TAG, "Index___MacAddress : " + _info.MACADDRESS + ", " + _info.MACADDRESS.length());
            }
        }
    }

    private void DRM_showInfo() {
        int nEndTime = _webServer.GetEndTime();
        int nCountPolicy = _webServer.GetPolicyCount();
        int nPolicyType = _webServer.GetPolicyType();

        if (nPolicyType == TMS4Encrypt.TMS4_POLICY_TYPE_PERIOD) // 기간 정책이 있는 경우.
        {
            if(VIEWLOG) Log.d(TAG, "policy type : period");
            if (nEndTime > 0) {
                long lTime = (long) nEndTime * 1000;
                if(VIEWLOG) Log.d(TAG, "time : " + Long.toString(lTime) + ", " + Integer.toString(nEndTime));
                Date date = new Date(lTime);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.KOREA);
                displayMessage(format.format(date) + " 까지 재생 가능");
            }
        }
        if (nPolicyType == TMS4Encrypt.TMS4_POLICY_TYPE_COUNT) // 횟수 정책이 있는 경우.
        {
            if (nCountPolicy > 0) {
                if(VIEWLOG) Log.d(TAG, "policy type : count");
                displayMessage("남은 횟수는  " + nCountPolicy + " 회");
            }
        }
        if (nPolicyType == (TMS4Encrypt.TMS4_POLICY_TYPE_PERIOD |
                TMS4Encrypt.TMS4_POLICY_TYPE_COUNT)) // 기간+횟수 정책이 있는 경우.
        {
            if(VIEWLOG) Log.d(TAG, "period + count");
            long lTime = (long) nEndTime * 1000;
            if(VIEWLOG) Log.d(TAG, "time : " + Long.toString(lTime) + ", " + Integer.toString(nEndTime));
            Date date = new Date(lTime);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.KOREA);
            if ((nEndTime > 0) && (nCountPolicy > 0))
                displayMessage(format.format(date) + " 까지 재생 가능하고 "
                        + nCountPolicy + " 회 재생 가능");
        }
    }

    /**********************************************************
     * MCM 관련
     **********************************************************/

    @Override
    protected void messageProcess(int msg, String packageName) {
        if(VIEWLOG) Log.e("TMCMSample", "messageProcess msg[" + msg + "]");
        String message = null;
        switch (msg) {
            case TMCM.MESSAGE_INIT:
                message = getString(R.string.msg_init);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_START:
                message = getString(R.string.msg_start);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_TIMEOUT:
                message = getString(R.string.msg_timeout);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_ROOTING:
                message = getString(R.string.msg_rooting);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_EMULATOR:
                message = getString(R.string.msg_emulator);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_HDMI:
                message = getString(R.string.msg_hdmi);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_MIRACAST:
                message = getString(R.string.msg_miracast);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_USB:
                message = getString(R.string.msg_usb);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_BLUETOOTH:
                message = getString(R.string.msg_bluetooth);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_DATANET:
                message = getString(R.string.msg_data);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_WIFI:
                message = getString(R.string.msg_wifi);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_PROCESSCHECK:
                message = getString(R.string.msg_process);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_REMOTECHECK:
                message = getString(R.string.msg_remote);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_GPS:
                message = getString(R.string.msg_gps);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_NONE:
            case TMCM.MESSAGE_READY:
                return;
            case TMCM.MESSAGE_SHORTKEY:
                Toast.makeText(this, "캡쳐가 되었습니다.", Toast.LENGTH_LONG).show();
                break;
            case TMCM.MESSAGE_RECORD:
                message = getString(R.string.msg_recode)+"\n"+packageName;
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                //finish();
                break;
            case TMCM.MESSAGE_AUDIO_RECORD	:
                message = getString(R.string.msg_audio_recode)+"\n"+packageName;
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                //finish();
                break;
            default:
                return;
        }
    }
}
