package com.teruten.download;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.os.storage.StorageManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.teruten.common.DRMINFO;
import com.teruten.common.Define;
import com.teruten.mcm.FirstSetPage;
import com.teruten.mcm.MCMFlags;
import com.teruten.sdcard.SdcardMCMAct;
import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayertestapp_tea_as.BuildConfig;
import com.teruten.tplayertestapp_tea_as.R;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static com.teruten.common.Define.DEVELOPER_MODE;

public class DownloadMCMActivity extends AppCompatActivity implements View.OnClickListener,
        ListView.OnItemClickListener,
        ListView.OnItemLongClickListener{

    //태그
    final public static String TAG = "TplayerTest";
    // 저장 경로
    final public static String strDefaultDownloadPath
            = Environment.getExternalStorageDirectory().getAbsolutePath() + "/teruten/";

    // listView
    private ListView listView;

    //목록 관련 오브젝트
    private List<String> mFileNames = new ArrayList<String>();
    private ArrayAdapter<String> listViewAdapter;

    //터치하는 동안 중복 터치 막기 위해.
    private boolean m_bTouch = false;

    // 임시 경로
    File downloadDir;

    //MCM
    private MCMFlags mMcmFlags = new MCMFlags();

    DRMINFO _info = DRMINFO.getInstance();

    // 다운로드 경로
    private String originalDownloadPath
            //= "http://122.199.199.140/files/android/original/sample_H1.mp4";
            //= "http://122.199.199.140/files/android/original/2gb_file.mkv";
            //= "http://122.199.199.140/files/android/original/6gb_file.mkv";
            = "http://122.199.199.140/files/android/original/Tangled.mp4";

    private String ms4DownloadPath
            //= "http://122.199.199.140/files/android/original/sample_H1.mp4.MS4"; /*프리패키징 MS4 파일 */
            //= "http://122.199.199.140/files/android/original/H_step1_ext_18_pre.mp4.MS4"; /*프리패키징 MS4 파일 */
            = "http://122.199.199.140/files/android/original/Coco_Sub_ep1.mp4.MS4"; /*프리패키징 MS4 파일 */
            //= "http://122.199.199.140/files/android/original/auth_yeskey.wmv.MS4"; /*프리패키징 MS4 파일 */
            //= "http://122.199.199.140/files/android/original/auth_6gb_file.mkv.MS4"; /*프리패키징 MS4 파일 */
            //= "http://122.199.199.140/files/android/original/Ninja.mp4.MS4"; /*프리패키징 MS4 파일 */

    // 다운로드 리스트 갱신을 위한 핸들러
    public Handler activityHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Define.MESSAGE_DOWNLOAD_COMPLETE:
                    mFileNames.clear();
                    updateFileList();
                    break;
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_mcm);

        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .build();

            StrictMode.setThreadPolicy(policy);
        }

        if (!checkPermissions() && Build.VERSION.SDK_INT >= 23) {
            requestPermissions();
        } else {
            // 초기화
            initLayout();
            setDeviceData();
        }

        // 다운로드 폴더가 없으면 생성
        downloadDir = new File(strDefaultDownloadPath);
        if (!downloadDir.exists())
            downloadDir.mkdirs();
        updateFileList();

        Log.e(TAG, "downloadDir : " + downloadDir.getPath());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // MCM Flag 받음
        if(resultCode == Define.MCM_SETTING_FLAG && data != null) {
            mMcmFlags = (MCMFlags)data.getSerializableExtra("mcmFlags");
        }
    }

    @Override
    public void onClick(View v) {

        if(DEVELOPER_MODE) _info.MACADDRESS = "90:2b:34:a2:3a:04"; // developer test

        _info.USERID = "UserA";
        _info.WATERMARK = "WatermarkA";
        _info.URL = "UrlA";
        _info.SELLERID = "SellerA";
        _info.SELLERCONTENTID = "CidA";

        switch(v.getId()) {
            case R.id.btn_mp4_download:
                new TMS4Download(this, activityHandler).execute(originalDownloadPath);
                break;

            case R.id.btn_ms4_download:
                new TMS4Download(this, activityHandler).execute(ms4DownloadPath);
                break;

            case R.id.btn_mcm_settings:
                Intent intent = new Intent(this, FirstSetPage.class);
                intent.putExtra("mcmFlags", mMcmFlags);
                intent.putExtra("type", Define.SDCARD_MCM_ACTIVITY_FLAG);
                startActivityForResult(intent, Define.MCM_SETTING_FLAG);
                break;

            case R.id.btn_list_update:
                mFileNames.clear();
                updateFileList();
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "long click.");
        String strPath = strDefaultDownloadPath + parent.getItemAtPosition(position).toString();
        Log.d(TAG, "path : " + strPath);
        File file = new File(strPath);
        file.delete();
        listViewAdapter.clear();
        updateFileList();
        displayMessage(strPath + "\r\ndelete");
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // 클릭시 영상 재생
        if (!m_bTouch) {
            m_bTouch = true;

            String strFilePath = downloadDir.getPath() + "/" + parent.getItemAtPosition(position).toString();
            playMedia(strFilePath);

            m_bTouch = false;
        }
    }

    // 파일 리스트 갱신
    public void updateFileList() {
        try {
            String ext = Environment.getExternalStorageState();
            String path = null;
            if (ext.equals(Environment.MEDIA_MOUNTED)) {
                path = downloadDir.getPath();
            } else {
                path = Environment.MEDIA_UNMOUNTED;
                displayMessage("SD카드를 찾을 수 없습니다.");
                return;
            }

            File files = new File(path);
            listViewAdapter = new ArrayAdapter<String>(this, R.layout.file_list_item, mFileNames);

            if (files.exists()) {
                File[] fileList = files.listFiles();
                if (fileList != null) {
                    for (File file : files.listFiles()) {
                        if (file.isFile())
                            mFileNames.add(file.getName());
                    }
                }
            }

            listView.setAdapter(listViewAdapter);
            listViewAdapter.notifyDataSetChanged();

        } catch (NullPointerException e) {
            displayMessage("다운로드 받은 파일이 없습니다.");
            Log.e("FileList", e.toString());
        } catch (Exception e) {
            displayMessage("Error Occured");
            Log.e("FileList", e.toString());
        }
    }

    // 미디어 플레이
    private void playMedia(String path) {
        Log.e(TAG, "play path: " + path);

        Intent intent = new Intent(this, VideoMCMAct.class);
        intent.putExtra("PATH",path);
        intent.putExtra("TITLE","title");
        intent.putExtra("rooting", mMcmFlags.isRooting());
        intent.putExtra("emulator", mMcmFlags.isEmulator());
        intent.putExtra("hdmi", mMcmFlags.isHDMI());
        intent.putExtra("miracast", mMcmFlags.isMiracast());
        intent.putExtra("usb", mMcmFlags.isUSB());
        intent.putExtra("bluetooth", mMcmFlags.isBluetooth());
        intent.putExtra("data", mMcmFlags.isData());
        intent.putExtra("wifi", mMcmFlags.isWiFi());
        intent.putExtra("process", mMcmFlags.isProcess());
        intent.putExtra("remote", mMcmFlags.isRemote());
        intent.putExtra("capturekey", mMcmFlags.isCaptureKey());
        intent.putExtra("gps", mMcmFlags.isGps());
        intent.putExtra("recode", mMcmFlags.isRecode());
        intent.putExtra("audiorecode", mMcmFlags.isAudioRecode());

        startActivity(intent);
    }

    // Toast 로 메시지 출력
    public void displayMessage(String message){
        if(message != null){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void initLayout()
    {
        Button btnOriginalDownload = (Button) findViewById(R.id.btn_mp4_download);
        Button btnMs4Download = (Button) findViewById(R.id.btn_ms4_download);
        Button btnListUpdate = (Button) findViewById(R.id.btn_list_update);
        Button btnMCMSetting = (Button) findViewById(R.id.btn_mcm_settings);
        btnOriginalDownload.setOnClickListener(this);
        btnMs4Download.setOnClickListener(this);
        btnListUpdate.setOnClickListener(this);
        btnMCMSetting.setOnClickListener(this);

        // 리스트에서 길게 누르면 파일 삭제
        listView = (ListView)findViewById(R.id.lv_listview);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) +
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProviceRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE) ||
                        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if( shouldProviceRationale ) {
            new AlertDialog.Builder(this)
                    .setTitle("INFO")
                    .setMessage("단말기 정보 및 저장소 권한이 필요합니다.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startPermissionRequest();
                        }
                    })
                    .create()
                    .show();
        } else {
            startPermissionRequest();
        }
    }

    private void startPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[] {Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, Define.REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case Define.REQUEST_PERMISSIONS_REQUEST_CODE:
                if( grantResults.length > 0) {
                    // 초기화
                    initLayout();
                    setDeviceData();
                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("INFO")
                            .setMessage("단말기 정보 및 저장소 권한이 필요합니다.")
                            .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null);
                                    intent.setData(uri);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            })
                            .create()
                            .show();
                }
        }
    }

    @SuppressLint("HardwareIds")
    private void setDeviceData() {
        TelephonyManager tpman = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiinfo = null;
        if (wifiManager != null) {
            wifiinfo = wifiManager.getConnectionInfo();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if(tpman == null && wifiinfo == null) return;

        _info.HINT = TMS4Encrypt.TMS4_HINT_PREPACK;

        if( tpman != null) {
            if (tpman.getDeviceId() != null && tpman.getDeviceId().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_IMEI;
                _info.IMEI = tpman.getDeviceId();
                Log.d(TAG, "Index___Imei : " + _info.IMEI + ", " + _info.IMEI.length());
            }
        }

        if(wifiinfo != null) {
            if (wifiinfo.getMacAddress() != null && wifiinfo.getMacAddress().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_MAC_ADDRESS;
                _info.MACADDRESS = wifiinfo.getMacAddress();

                Log.d(TAG, "Index___MacAddress : " + _info.MACADDRESS + ", " + _info.MACADDRESS.length());
            }
        }
    }
}
