package com.teruten.download;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.teruten.common.DRMINFO;
import com.teruten.common.Define;
import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tms4webserver.TMS4WebServer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

import static com.teruten.common.Define.VIEWLOG;
import static com.teruten.common.Define.VIEWLOG_WRITE;

public class TMS4Download extends AsyncTask<String, Integer, Integer>
{
	private Context mContext;
	private Handler mHandler;
	private ProgressDialog	lodingBar;
	private String	 FileName;
	private String	 FullPath;
	private boolean isMS4File;
	private long[]	nHandle;
	private long nDownloadPosition;
	private DRMINFO _info;

	//다운로드 파일 저장 상대 경로
	private String strDefaultDownloadPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/teruten/";

	// 초기화
	public TMS4Download(Context context, Handler handler)
	{
		mContext = context;
		mHandler = handler;
		isMS4File = false;

		// 파일 핸들 값
		nHandle = new long[1];
		nDownloadPosition = 0;

		// 디바이스, MAC, Hint 정보 가져오기
		_info = DRMINFO.getInstance();

		if(VIEWLOG) Log.i("TERUTEN", "CPU_ABI : " + Build.CPU_ABI);
        if(VIEWLOG) Log.i("TERUTEN", "CPU_ABI2 : " + Build.CPU_ABI2);
        if(VIEWLOG) Log.i("TERUTEN", "OS.ARCH : " + System.getProperty("os.arch"));
	}

	@Override
	protected void onPreExecute()
	{
		// 프로그레스바 초기화
		lodingBar = new ProgressDialog(mContext);
		lodingBar.setMessage("Downloading...");
		lodingBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		lodingBar.setMax(100);
		lodingBar.show();

		super.onPreExecute();
	}

	@Override
	protected void onPostExecute(Integer result)
	{
		// 다운로드 완료 후 프로그레스바 종료
		lodingBar.dismiss();
		if (result == 0)
		{
			// 다운로드 완료했다면 다운로드 리스트 갱신
			Message msg = Message.obtain(mHandler,
					Define.MESSAGE_DOWNLOAD_COMPLETE,
					0, 0);
			mHandler.sendMessage(msg);
			Toast.makeText(mContext, "Download Complete", Toast.LENGTH_SHORT).show();
		}
		else
		{
			Toast.makeText(mContext, "Download Fail : " + result, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onProgressUpdate(Integer... values)
	{
		// 다운로드 진행 동안 프로그레스바 갱신
		lodingBar.setProgress(values[0]);
	}

	@Override
	protected Integer doInBackground(String... params)
	{
        int responseCode = 0;

		if (params != null && params.length > 0)
		{
			// 경로 가져오기
			String UrlString = params[0];

			// MS4 파일인지 확인
			if(UrlString.endsWith(".MS4"))
			{
				isMS4File = true;
			}

			// 폴더 없으면 생성
			File path = new File(strDefaultDownloadPath);
			if (!path.exists())
				path.mkdirs();

			// 파일 이름 추출
			FileName = UrlString.substring(UrlString.lastIndexOf("/")+1, UrlString.length());

			// 최종 다운로드 받을 경로
			FullPath = strDefaultDownloadPath + FileName;

			// nDownloadPosition : 이전의 받은 위치
			if(isMS4File)
			{
				File tmpFile = new File(FullPath+".tmp");
				nDownloadPosition = tmpFile.length();
			}
			else
			{
				nDownloadPosition = TMS4Encrypt.TMS4EGetResumeFileSize(FullPath+".MS4");
			}

			// 한글일때는인식 못해서 아래처럼 파일 이름만 인코드해줌.
			try {
				UrlString = UrlString.substring(0, UrlString.lastIndexOf("/")+1) + URLEncoder.encode(FileName, "UTF-8").replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}

			try
			{
                if(VIEWLOG) Log.d("KJH", "UrlString = " + UrlString);

				// URL 연결
				URL url = new URL(UrlString);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();

				responseCode = conn.getResponseCode();
				if (responseCode == HttpURLConnection.HTTP_OK)
				{
                    if(VIEWLOG) Log.d("KJH", "responseCode = " + responseCode);
					int result = -1;

					// 파일 크기
					long fileSize = Long.parseLong(conn.getHeaderField("Content-Length"));
					if(fileSize <= nDownloadPosition) {
						if(!isMS4File) {
							result = TMS4Encrypt.TMS4ECreateFile(FullPath + ".MS4", nHandle);
							if (result != 0)
								return result;
						}

						result = PolicyBinding();
						return result;
					}

					// 크기 구하고 연결 끊기
					conn.disconnect();
                    if(VIEWLOG) Log.d("KJH", "fileSize = " + fileSize);

					// 다시연결
					conn = (HttpURLConnection) url.openConnection();

					// Range 설정
					conn.setRequestProperty("Range", "bytes=" + Long.toString(nDownloadPosition) + "-");

					// 파일 읽기
					result = readContent(conn.getInputStream(), fileSize, nDownloadPosition);
					if(result != 0)
						return result;

					result = PolicyBinding();
					return result;
				}
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
				return -10101;
			}
			catch (Exception e)
			{
				e.printStackTrace();
				return -100;
			}
		}

		return responseCode;
	}

	private int readContent(InputStream is, long totalSize, long currentSize)
	{
        if(VIEWLOG) Log.d("KJH", "totalSize = " + totalSize);

		int nRtn = 0;

		BufferedInputStream br = new BufferedInputStream(is);
		RandomAccessFile output = null;

		try
		{
			// MS4 파일이라면
			if(isMS4File)
			{
				// 파일 스트림 열기
				output = new RandomAccessFile(FullPath + ".tmp", "rw");

				// 파일 다운로드 마지막 인덱스 찾기
				output.seek(nDownloadPosition);
			}
			else
			{
				nRtn = TMS4Encrypt.TMS4ECreateFile(FullPath + ".MS4", nHandle);
				if(nRtn != 0)
				{
					return nRtn;
				}
			}
		}
		catch (FileNotFoundException e1)
		{
			e1.printStackTrace();
			return 1;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return 2;
		}

		byte[] data = new byte[4096];

		int bytesRead = 0;
		long totalRead = currentSize;

		try
		{
			// 데이터 읽기
			while ((bytesRead = br.read(data, 0, data.length)) >= 0)
			{
				if(isMS4File)
				{
					if(Thread.interrupted())
					{
						// 문제가 생기면 종료
						throw new InterruptedException();
					}
					// 데이터 쓰기
					output.write(data, 0, bytesRead);
				}
				else
				{
					if(Thread.interrupted())
					{
						// 문제가 생기면 핸들 닫고 종료
						TMS4Encrypt.TMS4ECloseHandle(nHandle[0]);
						throw new InterruptedException();
					}
					//실시간으로 버퍼에 담긴 데이터 암호화 하면서 파일 쓰기
					int bytesWrite = TMS4Encrypt.TMS4EWriteFile(data, bytesRead, nHandle[0]);
                    if(VIEWLOG_WRITE) Log.d("TERUTEN", "bytesWrite is " + bytesWrite);
				}

				totalRead += bytesRead;
				int percent = (int)(totalRead / ( totalSize / 100));
				publishProgress(percent);
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return 3;
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
			return 5;
		}
		finally
		{
			try
			{
                if(VIEWLOG) Log.d("KJH", "download stream close");
				if(isMS4File)
				{
					output.close();
				}
				br.close();
				is.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
				return 4;
			}
		}

		return 0;
	}

	private int PolicyBinding()
	{
		int nRtn = 0;
		int nStandardTime = (int)(System.currentTimeMillis() / 1000);

        if(VIEWLOG) Log.i("TMS4Download", "IMEI = " + _info.IMEI);
        if(VIEWLOG) Log.i("TMS4Download", "MACADDRESS = " + _info.MACADDRESS);

        long startTime = System.currentTimeMillis();


        if(isMS4File)
		{
			// MS4 파일이면 다운로드 완료 후 파일에 정책쓰기
			File file = new File(FullPath + ".tmp");
			file.renameTo(new File(FullPath));

            nRtn = TMS4Encrypt.TMS4ERecryptEx(FullPath, 			// 파일 경로
                    TMS4Encrypt.TMS4_POLICY_MAC,					// 정책 값
                    _info.IMEI,										// 기기 값
                    _info.MACADDRESS,								// 맥 어드레스 값
                    _info.USERID,									// 사용자 ID
                    _info.URL,										// 컨텐츠 URL
                    _info.WATERMARK,								// 워터마크정보
                    _info.UUID,										// uuid
                    _info.SELLERID,									// sellerId
                    _info.SELLERCONTENTID,							// sellerContentId
                    _info.COMPANYCODE,								// companyCode
                    nStandardTime,									// 현재시간
                    365,											// 날짜정책
                    0,											// 횟수정책
                    7);											// 서비스 레벨

			if(nRtn != 0)
			{
				return nRtn;
			}
		}
		else
		{
            // 원본파일을 암호화하며 다운로드 받고 파일 끝에 정책정보를 담은 Footer를 쓰기
			nRtn = TMS4Encrypt.TMS4EMakeFooter(nHandle[0],		// 파일 경로
					TMS4Encrypt.TMS4_POLICY_MAC,				// 정책 값
					_info.IMEI,									// 기기 값
					_info.MACADDRESS,							// 맥 어드레스 값
					_info.USERID,								// 사용자 ID
					_info.URL,									// 컨텐츠 URL
					_info.WATERMARK,							// 워터마크정보
					nStandardTime,								// 현재시간
					365,										// 날짜정책
					0,										// 횟수정책
					0);										// 서비스 레벨

			// 핸들 닫기
			TMS4Encrypt.TMS4ECloseHandle(nHandle[0]);
			if(nRtn != 0)
			{
				return nRtn;
			}
		}

        long endTime = System.currentTimeMillis();
        if(VIEWLOG) Log.d(TMS4WebServer.TAG, "start time : " + (startTime) );
        if(VIEWLOG) Log.d(TMS4WebServer.TAG, "start time : " + (endTime) );
        if(VIEWLOG) Log.d(TMS4WebServer.TAG, "recrypt time : " + (endTime - startTime) + "ms");

        return nRtn;
	}
}
