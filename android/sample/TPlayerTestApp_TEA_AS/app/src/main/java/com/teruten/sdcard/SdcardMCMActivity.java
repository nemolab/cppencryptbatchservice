package com.teruten.sdcard;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.os.storage.StorageManager;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.teruten.common.Define;
import com.teruten.download.ExoPlayerAct;
import com.teruten.mcm.FirstSetPage;
import com.teruten.mcm.MCMFlags;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayertestapp_tea_as.BuildConfig;
import com.teruten.tplayertestapp_tea_as.R;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static com.teruten.common.Define.VIEWLOG;

/**
 * 테스트 하기전에 디바이스에 Sdcard를 삽입해주세요.
 */
public class SdcardMCMActivity extends AppCompatActivity implements View.OnClickListener,
                                                                    ListView.OnItemClickListener,
                                                                    ListView.OnItemLongClickListener,
                                                                    RadioGroup.OnCheckedChangeListener{


    //태그
    final public static String TAG = "TplayerTest";

    private TextView tvSdcardInfo;

    // listView
    private ListView listView;

    //목록 관련 오브젝트
    private List<String> mFileNames = new ArrayList<>();
    private List<String> mFileNamePath = new ArrayList<>();

    //터치하는 동안 중복 터치 막기 위해.
    private boolean m_bTouch = false;

    // 임시 경로
    File downloadDir;

    //MCM
    private MCMFlags mMcmFlags = new MCMFlags();

    private boolean _useExoPlayer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdcard_mcm);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .build();

            StrictMode.setThreadPolicy(policy);
        }

        // 초기화
        initLayout();

        String strRemovableSdcardPath = getStoragePath(this, true);
        if(strRemovableSdcardPath == null) {
            strRemovableSdcardPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/teruten/";
        }

        // Sdcard 에 다운로드 폴더 없으면 생성
        //downloadDir = new File(strRemovableSdcardPath + "/Android/data/");
        downloadDir = new File(strRemovableSdcardPath + "/Teruten/");
        //downloadDir = new File(strRemovableSdcardPath);
        if (!downloadDir.exists())
            Toast.makeText(this, "폴더 없음", Toast.LENGTH_SHORT).show();

        if (!checkPermissions() && Build.VERSION.SDK_INT >= 23) {
            requestPermissions();
        } else {
            updateFileList();
        }

        if(VIEWLOG) Log.e(TAG, "downloadDir : " + downloadDir.getPath());
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch(checkedId) {
            case R.id.rdbDefaultVideo:
                _useExoPlayer = false;
                break;

            case R.id.rdbExoplayer:
                _useExoPlayer = true;
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // MCM Flag 받음
        if(resultCode == Define.MCM_SETTING_FLAG && data != null) {
            mMcmFlags = (MCMFlags)data.getSerializableExtra("mcmFlags");
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_sdcard_info:
                TMS4WebServer ws = new TMS4WebServer();
                String serial = ws.getSDcardSerial();
                if(!serial.equals("")) {
                    Log.d(TMS4WebServer.TAG, serial);
                    tvSdcardInfo.setText(serial);
                }
                else
                    tvSdcardInfo.setText("Sdcard를 확인해주시기 바랍니다.");
                break;

            case R.id.btn_mcm_settings:
                android.content.Intent intent = new Intent(this, FirstSetPage.class);
                intent.putExtra("mcmFlags", mMcmFlags);
                intent.putExtra("type", Define.SDCARD_MCM_ACTIVITY_FLAG);
                startActivityForResult(intent, Define.MCM_SETTING_FLAG);
                break;

            case R.id.btn_list_update:
                mFileNames.clear();
                updateFileList();
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // 클릭시 영상 재생
        if(!m_bTouch)
        {
            m_bTouch = true;

            playMedia(mFileNamePath.get(position));

            m_bTouch = false;
        }
    }

    // 파일 리스트 갱신
    public void updateFileList(){
        try {
            String ext = Environment.getExternalStorageState();
            String path = null;
            if(ext.equals(Environment.MEDIA_MOUNTED)){
                path = downloadDir.getPath();
            }

            else
            {
                path = Environment.MEDIA_UNMOUNTED;
                displayMessage("SD카드를 찾을 수 없습니다.");
                return;
            }

            ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(this, R.layout.file_list_item, mFileNames);

            subDir(path);

            Toast.makeText(this, mFileNamePath.size() + "", Toast.LENGTH_SHORT).show();
            listView.setAdapter(listViewAdapter);
            listViewAdapter.notifyDataSetChanged();

        } catch (NullPointerException e) {
            displayMessage("다운로드 받은 파일이 없습니다.");
            if(VIEWLOG) Log.e("FileList", e.toString());
        } catch (Exception e){
            displayMessage("Error Occured");
            if(VIEWLOG) Log.e("FileList", e.toString());
        }
    }

    // 미디어 플레이
    private void playMedia(String path) {
        if(VIEWLOG) Log.e(TAG, "play path: " + path);

        Intent intent = null;

        if(_useExoPlayer) {
            intent = new Intent(this, ExoPlayerAct.class);
            intent.putExtra("drmType", Define.SDCARD);
        }
        else {
            intent = new Intent(this, SdcardMCMAct.class);
        }

        intent.putExtra("PATH",path);
        intent.putExtra("TITLE","title");
        intent.putExtra("rooting", mMcmFlags.isRooting());
        intent.putExtra("emulator", mMcmFlags.isEmulator());
        intent.putExtra("hdmi", mMcmFlags.isHDMI());
        intent.putExtra("miracast", mMcmFlags.isMiracast());
        intent.putExtra("usb", mMcmFlags.isUSB());
        intent.putExtra("bluetooth", mMcmFlags.isBluetooth());
        intent.putExtra("data", mMcmFlags.isData());
        intent.putExtra("wifi", mMcmFlags.isWiFi());
        intent.putExtra("process", mMcmFlags.isProcess());
        intent.putExtra("remote", mMcmFlags.isRemote());
        intent.putExtra("capturekey", mMcmFlags.isCaptureKey());
        intent.putExtra("gps", mMcmFlags.isGps());
        intent.putExtra("recode", mMcmFlags.isRecode());
        intent.putExtra("audiorecode", mMcmFlags.isAudioRecode());

        startActivity(intent);
    }

    // Toast 로 메시지 출력
    public void displayMessage(String message){
        if(message != null){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void initLayout()
    {
        // Sdcard 정보 출력
        tvSdcardInfo = (TextView)findViewById(R.id.tv_sdcard_info);

        // Sdcard 정보 추출
        // layout
        Button btnSdcardInfo = (Button) findViewById(R.id.btn_sdcard_info);
        Button btnMCMSetting = (Button) findViewById(R.id.btn_mcm_settings);
        Button btnListUpdate = (Button) findViewById(R.id.btn_list_update);
        btnSdcardInfo.setOnClickListener(this);
        btnMCMSetting.setOnClickListener(this);
        btnListUpdate.setOnClickListener(this);

        // 리스트에서 길게 누르면 파일 삭제
        listView = (ListView)findViewById(R.id.lv_listview);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        RadioGroup rdG = (RadioGroup) findViewById(R.id.rd_radio_group);
        rdG.setOnCheckedChangeListener(this);
    }

    private static String getStoragePath(Context context, boolean is_removale) {

        StorageManager storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        Class<?> storageVolumeClass = null;
        try {
            storageVolumeClass = Class.forName("android.os.storage.StorageVolume");
            Method getVolumeList = storageManager.getClass().getMethod("getVolumeList");
            Method getPath = storageVolumeClass.getMethod("getPath");
            Method isRemovable = storageVolumeClass.getMethod("isRemovable");
            Object result = getVolumeList.invoke(storageManager);
            final int length = Array.getLength(result);
            for (int i = 0; i < length; i++) {
                Object storageVolumeElement = Array.get(result, i);
                String path = (String) getPath.invoke(storageVolumeElement);
                boolean removable = (Boolean) isRemovable.invoke(storageVolumeElement);
                if (is_removale == removable) {
                    return path;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) +
                                ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProviceRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO);

        if( shouldProviceRationale ) {
            new AlertDialog.Builder(this)
                    .setTitle("INFO")
                    .setMessage("외장 sdcard 읽기 및 마이크 권한이 필요합니다. 환경 설정에서 저장공간 및 마이크 권한을 허가해주세요.")
                    .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startPermissionRequest();
                        }
                    })
                    .create()
                    .show();
        } else {
            startPermissionRequest();
        }
    }

    private void startPermissionRequest() {
        ActivityCompat.requestPermissions(this,
                new String[] { Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.RECORD_AUDIO } , Define.REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case Define.REQUEST_PERMISSIONS_REQUEST_CODE:
                if( grantResults.length > 0) {
                    boolean fileReadPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean audioRecodePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if(fileReadPermission && audioRecodePermission)
                        updateFileList();

                } else {
                    new AlertDialog.Builder(this)
                            .setTitle("알림")
                            .setMessage("외장 sdcard 읽기 및 마이크 권한이 필요합니다. 환경 설정에서 저장공간 및 마이크 권한을 허가해주세요.")
                            .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package",
                                            BuildConfig.APPLICATION_ID, null);
                                    intent.setData(uri);
                                    intent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            })
                            .create()
                            .show();
                }
        }
    }

    private void subDir(String path) {
        File dir = new File(path);
        File[] fileList = dir.listFiles();
        try{
            for(int i = 0 ; i < fileList.length ; i++){
                File file = fileList[i];
                if(file.isFile()){
                    if(file.getName().endsWith(".MS4")) {
                        mFileNames.add(file.getName());
                        mFileNamePath.add(file.getAbsolutePath());
                    }
                }else if(file.isDirectory()){
                    if(file.getName().equals("audio") || file.getName().equals("image") || file.getName().equals("image_old")
                            || file.getName().startsWith("com."))
                        continue;

                    subDir(file.getCanonicalPath().toString());
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
