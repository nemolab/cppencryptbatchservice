package com.teruten.common;

/**
 * Created by KIM on 2017-11-10.
 */

public class Define {
    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 99;              /* 권한*/
    public static final int MCM_SETTING_FLAG = 1011;                             /* MCM */
    public static final int MESSAGE_DOWNLOAD_COMPLETE = 1001;                   /* 다운로드 완료 */
    public static final int SDCARD_MCM_ACTIVITY_FLAG = 10111;
    public static final int DOWNLOAD_MCM_ACTIVITY_FLAG = 10112;
    public static final int DOWNLOAD = 2000;
    public static final int SDCARD = 2001;
    public static final int STREAMING = 2002;
    public static final int PREPACKAGING = 2003;

    public static final boolean DEVELOPER_MODE = false;             /* PC에서 복호화 테스트하기 위함 */
    public static final boolean VIEWLOG = true;
    public static final boolean VIEWLOG_WRITE = false;
}