package com.teruten.download;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.teruten.common.DRMINFO;
import com.teruten.mcm.TMCM;
import com.teruten.mcm.TMCMActivity;
import com.teruten.tms4encrypt.TMS4Encrypt;
import com.teruten.tms4webserver.TMS4WebServer;
import com.teruten.tplayertestapp_tea_as.R;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static com.teruten.common.Define.DEVELOPER_MODE;

public class VideoMCMAct extends TMCMActivity {

    VideoView _video = null;
    private TMS4WebServer _webServer;
    String TAG = "VideoDRMAct";

    DRMINFO _info = DRMINFO.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video);

        _video = (VideoView) findViewById(R.id.video);
        MediaController mc = new MediaController(this);
        _video.setMediaController(mc);

        String strMediaPath = getIntent().getStringExtra("PATH");

        /***************************** DRM ***********************/
        if (strMediaPath.endsWith(".MS4")) {
            int nReturn;

            // 초기화
            if (DRM_Init() != TMS4Encrypt.TMS4E_OK) return;

            // 파일 인증받기 - 원격 파일
            if (strMediaPath.startsWith("http://") || strMediaPath.startsWith("https://")) {
                nReturn = _webServer.OpenRemoteMedia(strMediaPath, "SiwonS01");
                if (nReturn != TMS4Encrypt.TMS4E_OK) {
                    displayMessage("open remote media error : " + nReturn);
                    return;
                }
            }
            // 파일 인증받기 - 로컬 파일
            else {
                int nTime = (int) (System.currentTimeMillis() / 1000) + 1;
                nReturn = _webServer.MediaAccreditation(strMediaPath, nTime);
                if (nReturn != 0) {
                    displayMessage("open local media error : " + nReturn);
                    Log.d(TAG, "Faile Accredit : " + Integer.toString(nReturn));
                    return;
                }

                DRM_showInfo();
            }

            // DRM 용 파일 경로 가져오기.
            strMediaPath = _webServer.getFilePath(strMediaPath);
        }

        _video.setVideoPath(strMediaPath);
        _video.start();
        Log.e("VideoAct", "media path : " + strMediaPath);

        _video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            public void onCompletion(MediaPlayer mp) {
                Log.e("VideoAct", "completion.");
            }
        });
        _video.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e("VideoAct", "error.");
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "video act onDestroy");
        if (_webServer != null) {
            try {
                _webServer.StopServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
            _webServer = null;
        }

        super.onDestroy();
    }


    /**********************************************************
     * DRM 관련 함수
     **********************************************************/
    private int DRM_Init() {
        int nReturn = -1;

        if (_webServer == null) {
            Log.e(TAG, "web server start");
            _webServer = new TMS4WebServer();

            setDeviceData();
            DRMINFO info = DRMINFO.getInstance();

            if(DEVELOPER_MODE) info.MACADDRESS = "90:2b:34:a2:3a:04"; // developer test

            nReturn = _webServer.InitInstance(info.IMEI, info.MACADDRESS, this);
            if (nReturn != TMS4Encrypt.TMS4E_OK) {
                displayMessage("DRM init err : " + nReturn);
                Log.e(TAG, "DRM init err : " + nReturn);
                return nReturn;
            }
            _webServer.StartServer(50000);
        }
        return nReturn;
    }

    private void DRM_showInfo() {
        int nEndTime = _webServer.GetEndTime();
        int nCountPolicy = _webServer.GetPolicyCount();
        int nPolicyType = _webServer.GetPolicyType();

        if (nPolicyType == TMS4Encrypt.TMS4_POLICY_TYPE_PERIOD) // 기간 정책이 있는 경우.
        {
            Log.d(TAG, "policy type : period");
            if (nEndTime > 0) {
                long lTime = (long) nEndTime * 1000;
                Log.d(TAG, "time : " + Long.toString(lTime) + ", " + Integer.toString(nEndTime));
                Date date = new Date(lTime);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.KOREA);
                displayMessage(format.format(date) + " 까지 재생 가능");
            }
        }
        if (nPolicyType == TMS4Encrypt.TMS4_POLICY_TYPE_COUNT) // 횟수 정책이 있는 경우.
        {
            if (nCountPolicy > 0) {
                Log.d(TAG, "policy type : count");
                displayMessage("남은 횟수는  " + nCountPolicy + " 회");
            }
        }
        if (nPolicyType == (TMS4Encrypt.TMS4_POLICY_TYPE_PERIOD |
                TMS4Encrypt.TMS4_POLICY_TYPE_COUNT)) // 기간+횟수 정책이 있는 경우.
        {
            Log.d(TAG, "period + count");
            long lTime = (long) nEndTime * 1000;
            Log.d(TAG, "time : " + Long.toString(lTime) + ", " + Integer.toString(nEndTime));
            Date date = new Date(lTime);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.KOREA);
            if ((nEndTime > 0) && (nCountPolicy > 0))
                displayMessage(format.format(date) + " 까지 재생 가능하고 "
                        + nCountPolicy + " 회 재생 가능");
        }

        if(nPolicyType == TMS4Encrypt.TMS4_HINT_PREPACK) {
            displayMessage("무제한 정책");
        }
    }

    private void finishVideo() {
        _video.stopPlayback();
        this.finish();
    }

    /**********************************************************
     * 기타 함수
     **********************************************************/
    // Toast 로 메시지 출력
    public void displayMessage(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("HardwareIds")
    private void setDeviceData() {
        TelephonyManager tpman = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        @SuppressLint("WifiManagerLeak") WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifiinfo = null;
        if (wifiManager != null) {
            wifiinfo = wifiManager.getConnectionInfo();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if(tpman == null && wifiinfo == null) return;

        _info.HINT = TMS4Encrypt.TMS4_HINT_PREPACK;

        if( tpman != null) {
            if (tpman.getDeviceId() != null && tpman.getDeviceId().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_IMEI;
                _info.IMEI = tpman.getDeviceId();
                Log.d(TAG, "Index___Imei : " + _info.IMEI + ", " + _info.IMEI.length());
            }
        }

        if(wifiinfo != null) {
            if (wifiinfo.getMacAddress() != null && wifiinfo.getMacAddress().length() > 0) {
                _info.HINT = _info.HINT | TMS4Encrypt.TMS4_HINT_MAC_ADDRESS;
                _info.MACADDRESS = wifiinfo.getMacAddress();
                Log.d(TAG, "Index___MacAddress : " + _info.MACADDRESS + ", " + _info.MACADDRESS.length());
            }
        }
    }

    /**********************************************************
     * MCM 관련
     **********************************************************/

    @Override
    protected void messageProcess(int msg, String packageName) {
        Log.e("TMCMSample", "messageProcess msg[" + msg + "]");
        String message = null;
        switch (msg) {
            case TMCM.MESSAGE_INIT:
                message = getString(R.string.msg_init);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_START:
                message = getString(R.string.msg_start);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_TIMEOUT:
                message = getString(R.string.msg_timeout);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_ROOTING:
                message = getString(R.string.msg_rooting);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_EMULATOR:
                message = getString(R.string.msg_emulator);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_HDMI:
                message = getString(R.string.msg_hdmi);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_MIRACAST:
                message = getString(R.string.msg_miracast);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_USB:
                message = getString(R.string.msg_usb);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_BLUETOOTH:
                message = getString(R.string.msg_bluetooth);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_DATANET:
                message = getString(R.string.msg_data);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_WIFI:
                message = getString(R.string.msg_wifi);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_PROCESSCHECK:
                message = getString(R.string.msg_process);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_REMOTECHECK:
                message = getString(R.string.msg_remote);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_GPS:
                message = getString(R.string.msg_gps);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                break;
            case TMCM.MESSAGE_NONE:
            case TMCM.MESSAGE_READY:
                return;
            case TMCM.MESSAGE_SHORTKEY:
                Toast.makeText(this, "캡쳐가 되었습니다.", Toast.LENGTH_LONG).show();
                break;
            case TMCM.MESSAGE_RECORD:
                message = getString(R.string.msg_recode) + "\n" + packageName;
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                //finish();
                break;
            case TMCM.MESSAGE_AUDIO_RECORD:
                message = getString(R.string.msg_audio_recode) + "\n" + packageName;
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                //finish();
                break;
            default:
                return;
        }
    }
}
