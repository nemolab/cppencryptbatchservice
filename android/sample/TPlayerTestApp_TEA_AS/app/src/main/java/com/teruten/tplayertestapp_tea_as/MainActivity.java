package com.teruten.tplayertestapp_tea_as;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.teruten.authentication.OnlineAuthenticationActivity;
import com.teruten.download.DownloadActivity;
import com.teruten.download.DownloadMCMActivity;
import com.teruten.download.LocalRecryptActivity;
import com.teruten.download.StreamingActivity;
import com.teruten.sdcard.SdcardActivity;
import com.teruten.sdcard.SdcardMCMActivity;
import com.teruten.vr360.Vr360Activity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layoutInit();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode== android.view.KeyEvent.KEYCODE_BACK)
        {
            ExitMsgPop();
            return(true);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sdcard:
                startActivity(new Intent(this, SdcardActivity.class));
                break;

            case R.id.btn_sdcard_mcm:
                startActivity(new Intent(this, SdcardMCMActivity.class));
                break;

            case R.id.btn_download:
                startActivity(new Intent(this, DownloadActivity.class));
                break;

            case R.id.btn_download_mcm:
                startActivity(new Intent(this, DownloadMCMActivity.class));
                break;

            case R.id.btn_streaming:
                startActivity(new Intent(this, StreamingActivity.class));
                break;

            case R.id.btn_vr_download:
                startActivity(new Intent(this, Vr360Activity.class));
                break;

            case R.id.btn_local_recrypt:
                startActivity(new Intent(this, LocalRecryptActivity.class));
                break;

            case R.id.btn_online_authentication:
                startActivity(new Intent(this, OnlineAuthenticationActivity.class));
                break;
        }
    }

    private void layoutInit() {
        Button btnSdcard = (Button) findViewById(R.id.btn_sdcard);
        Button btnSdcardMCM = (Button) findViewById(R.id.btn_sdcard_mcm);
        Button btnDownload = (Button) findViewById(R.id.btn_download);
        Button btnDownloadMCM = (Button) findViewById(R.id.btn_download_mcm);
        Button btnStreaming = (Button) findViewById(R.id.btn_streaming);
        Button btnVrDownload = (Button) findViewById(R.id.btn_vr_download);
        Button btnLocalRecrypt = (Button) findViewById(R.id.btn_local_recrypt);
        Button btnOnlineAuthentication = (Button) findViewById(R.id.btn_online_authentication);

        btnSdcard.setOnClickListener(this);
        btnSdcardMCM.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        btnDownloadMCM.setOnClickListener(this);
        btnStreaming.setOnClickListener(this);
        btnVrDownload.setOnClickListener(this);
        btnLocalRecrypt.setOnClickListener(this);
        btnOnlineAuthentication.setOnClickListener(this);
    }

    private void ExitMsgPop()
    {
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        alt_bld.setMessage("종료하시겠습니까?").setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    // yes
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            // no
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = alt_bld.create();
        alert.show();
    }
}
